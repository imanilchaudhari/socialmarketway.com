import { app } from './app';

app.helper = {};

app.helper.isObjectEmpty = function(obj) {
	return Object.keys(obj).length === 0;
};

app.helper.isNumber = function(val) {
	val = parseInt(val);
	return val === (val^0);
};

app.helper.getPageId = function() {
	return parseInt($('[page-id]').attr('page-id'));
}

app.helper.showResponseMessage = function(response) {
	response.isSuccessful
	? toastr.success(response.message || "Done.")
	: toastr.error(response.message || "Error occured.");
}

app.helper.getCurrentAndVersioningSlugs = function(image_id = null, page_id = null) {
	var dataToSend = {image_id: image_id, page_id : page_id};
	
	$.getJSON(window.Yii.app.baseUrl+'page/get-current-and-versioning-slugs', dataToSend)
	.then(function(res) {
		window.prompt("Please press CTRL + C to copy the slug/versionings below:", res.slugs);
	});
};

app.helper.publishRightNow = function(page_id) {
	$.getJSON(window.Yii.app.baseUrl+'page/publish-now', {id: page_id})
	.then(function(res) {
		app.helper.showResponseMessage(res);
	});
};

app.helper.updatePageScheduleTimeOnButton = function(page_id) {
	$.get(window.Yii.app.baseUrl+'page/get-scheduled-date-time', {page_id: page_id})
	.then(function(res) {
		$(".page-schedule-button .time").text(res);
	});
};

app.helper.schedulePage = function(page_id, datetime_field_name) {
	var publish_date_time_string = $('[name="'+datetime_field_name+'"]').val();
	
	$.getJSON(
		window.Yii.app.baseUrl+'page/schedule',
		{page_id, publish_date_time_string}
	).then(function(res) {
		if (res.isSuccessful) {
			toastr.success(res.message || "Done.");
		} else {
			for (var key in res.message) {
				toastr.error(res.message[key]);
			}
		}
	})
	.always(function() { app.helper.updatePageScheduleTimeOnButton(page_id); });
};
app.helper.unschedulePage = function(page_id) {
	$.getJSON(window.Yii.app.baseUrl+'page/unschedule', {page_id: page_id})
	.then(function(res) { app.helper.showResponseMessage(res); })
	.always(function() { app.helper.updatePageScheduleTimeOnButton(page_id); });
};

app.helper.seoFieldsHelperInit = function() {
	$(".title-content, .title-content-seo, .field-keyword-seo_title").each(function() {
		$(this)
		.off("keyup keydown change focus")
		.on("keyup keydown change focus", function (event) {
			var charLength = $('input', this).val().length;
			var messageContainer = $(this).find(".help-block");
			if (messageContainer.length === 0) {
				$(this).append("<p class='help-block'></p>");
				messageContainer = $(this).find(".help-block");
			}
			
			messageContainer.html( "<i style=\"color:green\">Character Count : " + charLength + "</i>" );
			
			var charLimit = parseInt(69);
			if ( charLength > charLimit ) {
				messageContainer.html("<i style=\"color:red\">Character Count : "+ charLength +" Please try to keep character-length below " + charLimit + " characters if possible.</i>");
			}
		});
	});
	
	$(".seo-description, #keyword-seo_description").each(function() {
		$(this)
		.off("keyup keydown change focus")
		.on("keyup keydown change focus", function (event) {
			var charLength = $(this).val().length;
			var messageContainer = $(this).next(".char-counter");
			if (messageContainer.length == 0) {
				$(this).after("<p class=\"char-counter\"></p>");
			}
			messageContainer = $(this).next(".char-counter");
			messageContainer.html( "<i style=\"color:green\">Character Count : " + charLength + "</i>" );
			
			var charLimit = parseInt(140);
			if ( charLength > charLimit ) {
				messageContainer.html("<i style=\"color:red\">Character Count : "+ charLength +" Please try to keep character-length below " + charLimit + " characters if possible.</i>");
			}
		});
	});
	
	
	$(".image-seo-title").each(function() {
		$(this)
		.off("keyup keydown change focus")
		.on("keyup keydown change focus", function (event) {
			var charLength = $(this).val().length;
			var messageContainer = $(this).next(".char-counter");
			if (messageContainer.length == 0) {
				$(this).after("<p class=\"char-counter\"></p>");
			}
			messageContainer = $(this).next(".char-counter");
			messageContainer.html( "<i style=\"color:green\">Character Count : " + charLength + "</i>" );
			
			var charLimit = parseInt(69);
			if ( charLength > charLimit ) {
				messageContainer.html("<i style=\"color:red\">Character Count : "+ charLength +" Please try to keep character-length below " + charLimit + " characters if possible.</i>");
			}
		});
	});
};

app.helper.getWordCount = function(contents) {
	var contents = contents.replace(/<[^>]*>/g," ");
	contents = contents.replace(/\s+/g, ' ');
	contents = contents.trim();
	return contents.split(" ").length
}

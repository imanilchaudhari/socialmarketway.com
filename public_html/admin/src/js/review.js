import { app } from './app';

app.review = {};
app.review.editPanelModal = {};
app.review.panelContainerSelector = "";
app.review.currentPageId = "";

app.review.managePanel = {
	pageId: '',
	containerSelector: '',
	setPageId: function(id) {
		this.pageId = id;
	},
	getPageId: function() {
		return this.pageId;
	},
	setContainerSelector: function(selector) {
		this.containerSelector = selector;
	},
	getContainerSelector: function() {
		return this.containerSelector;
	},
	getContainer: function() {
		return $(this.getContainerSelector());
	},
	getEditModal: function() {
		return $('#reviewManageModal');
	},
	getModalBody: function() {
		return $('.modal-body', this.getEditModal());
	},
	showModal: function() {
		this.getEditModal().modal('show');
	},
	hideModal: function() {
		this.getEditModal().modal('hide');
	},
	getTogglePublishLinks: function() {
		return this.getContainer().find('[name="toggle-publish-status"]');
	},
	enableSwitcheryOnTogglePublishLinks: function() {
		this.getTogglePublishLinks().each(function() {
			new Switchery(this);
		});
		this.handleTogglePublishLinkChange();
	}
};

app.review.managePanel.load = function(containerSelector, page_id) {
	var self = app.review.managePanel;
	var panelContainer = $(containerSelector);
	self.setContainerSelector(containerSelector);
	self.setPageId(page_id);
	
	self.getContainer().showLoading();
	
	$.get(window.Yii.app.baseUrl+'review/get-manage-panel?page_id='+page_id)
	.then(function(panelHtml) {
		if (panelHtml.length) {
			self.getContainer().html(panelHtml);
			self.enableSwitcheryOnTogglePublishLinks();
			self.enableRateYo();
		}
	})
	.always(function() {
		self.getContainer().hideLoading();
	});
};

app.review.managePanel.reload = function() {
	var self = app.review.managePanel;
	self.load(self.getContainerSelector(), self.getPageId());
};

app.review.managePanel.showEditPanel = function(review_id = null) {
	var self = app.review.managePanel;
	self.getModalBody().load(window.Yii.app.baseUrl+'review/get-edit-panel?review_id='+review_id);
	self.showModal();
};

app.review.managePanel.showSchedulePanel = function(review_id) {
	var self = app.review.managePanel;
	self.getModalBody().load(window.Yii.app.baseUrl+'review/get-schedule-panel?review_id='+review_id);
	self.showModal();
};

app.review.managePanel.deleteReview = function(id) {
	if (!window.confirm('Are you sure?')) return 0;
	
	$.post(window.Yii.app.baseUrl+'review/delete-via-ajax', {id: id})
	.then(function(response) {
		app.helper.showResponseMessage(response);
		app.review.managePanel.reload();
	});
};

app.review.managePanel.save = function(formClass) {
	var editForm = $(formClass);
	var dataToSend = editForm.find("[name^=Review]").serializeArray();
	dataToSend.push(
		{name: '_csrf', value: yii.getCsrfToken()},
		{name: 'Review[page_id]', value: app.helper.getPageId()}
	);
	
	$.post(window.Yii.app.baseUrl+'review/save-edit', dataToSend)
	.then(function(response) {
		if (response.isSuccessful) {
			app.review.managePanel.hideModal();
			app.review.managePanel.reload();
		}
	});
};

app.review.managePanel.handleTogglePublishLinkChange = function() {
	app.review.managePanel.getTogglePublishLinks().change(function() {
		var reviewId = $(this).data('review-id');
		
		$.getJSON(window.Yii.app.baseUrl+'review/toggle-publish-status?id='+reviewId)
		.then( function(response) {
			app.helper.showResponseMessage(response);
		});
	});
};

app.review.managePanel.getFacebookGrabber = function(page_id) {
	var self = app.review.managePanel;
	self.getModalBody().load(window.Yii.app.baseUrl+'review/get-facebook-grabber?page_id='+page_id);
	self.showModal();
};

app.review.managePanel.enableRateYo = function() {
	$('.review-rating').each(function() {
		var ratingContainer = $(this);
		var reviewId = ratingContainer.data('review-id');
		var ratingGiven = ratingContainer.data('rating');
		ratingContainer.rateYo({
			starWidth: "20px",
			halfStar: true,
			onSet: function (rating, rateYoInstance) {
				$.post(
					window.Yii.app.baseUrl+'review/set-rating',
					{
						id: reviewId,
						rating: rating,
						_csrf: yii.getCsrfToken()
					}
				)
				.then(function(res) {
					app.helper.showResponseMessage(res);
				})
			}
		});
	});
};

app.review.managePanel.refresh = function() {
	var managePanel = app.review.managePanel;
	managePanel.getModalBody().showLoading();
	app.review.addNew();
	setTimeout(function() {
		managePanel.getModalBody().hideLoading();
	}, 1000);
};

app.review.addNew = function() {
	app.review
	.getOneUnassignedReviewId()
	.then(function(id) {
		app.helper.isNumber(id)
		? app.review.managePanel.showEditPanel(id)
		: toastr.error("Review not received. Please contact the developer team.");
	});
};

app.review.getOneUnassignedReviewId = function() {
	return $.get(window.Yii.app.baseUrl+'review/get-one-unassigned-review-id');
};



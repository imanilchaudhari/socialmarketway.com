import { app } from './app';

app.selectize = {};

app.selectize.get = function(field) {
    return field.selectize()[0].selectize;
};

app.selectize.addItems = function (field, items) {
    var selectizeInstance = app.selectize.get(field);

    $.each(items, function (key) {
        var itemValue = items[key];
        var item = {[key] : itemValue};
        app.selectize.addItem(field, item);
    });
};

app.selectize.addItem = function(field, item) {
    if (item) {
        var selectizeInstance = app.selectize.get(field);
        var itemId = item.id || Object.keys(item)[0];
        var itemValue = item.text || item[itemId];

        selectizeInstance.addOption({id: itemId, text: itemValue});
        selectizeInstance.addItem(itemId);
    }
};

app.selectize.getId = function(field) {
    var selectizeInstance = app.selectize.get(field);
    return selectizeInstance.getValue();
};

app.selectize.getText = function(field) {
    var selectizeInstance = app.selectize.get(field);
    var val = app.selectize.getId(field);
    return selectizeInstance.getItem(val).text();
};

app.selectize.on = function (field, itemListUrl, extraDataToSend, newItemCreatable = false) {
    var config = {
    valueField: 'id',
    labelField: 'text',
    searchField: ['id', 'text'],
    plugins: [],
    load: function (query, callback) {
        if (!query.length) return callback();
        var dataToSend = {q: query};
        $.each(extraDataToSend, function(name, value) {
        dataToSend[name] = value;
        });

        $.getJSON(Yii.app.baseUrl + itemListUrl, dataToSend)
        .done(function (response)      { callback(response.results);                })
        .fail(function(XMLHttpRequest) { toastr.error(XMLHttpRequest.responseText); });
    }
    };

    if ( $(field).is('[multiple]') )
    config['plugins'].push('remove_button');

    if (newItemCreatable === true) {
        config['create'] = function(query, callback) {
            callback({id: 0, text: query});
        };
    }

    field.selectize(config);
};

app.selectize.pageType = function(fields) {
    fields.each( function(index, field) {
        app.selectize.on($(field), 'placement-used/get-page-type');
    });
};

app.selectize.placement = function(fields) {
    fields.each( function(index, field) {
        app.selectize.on($(field), 'placement-used/get-placement');
    });
};

app.selectize.getPages = function(fields, type) {
    fields.each( function(index, field) {
        app.selectize.on($(field), 'page/get-pages',{type:type});
    });
};

app.selectize.dataentry = function(fields) {
    fields.each( function(index, field) {
        app.selectize.on($(field), 'ajax/data-entry-list');
    });
};

app.selectize.country = function (fields) {
    fields.each( function(index, field) {
        app.selectize.on($(field), 'country/country-list');
    });
}

app.selectize.countryLan = function(fields) {
    fields.each(function(index, field) {
        app.selectize.on($(field), 'country/language-list')
    });
}

app.selectize.languageCode =function(fields) {
    fields.each(function(index, field) {
        app.selectize.on($(field), 'country/language-code-list')
    });
}

app.selectize.off = function(fields) {
    fields.each(function(key, field) {
        app.selectize.get($(field)).destroy();
    })
};

app.selectize.reset = function(fields) {
    fields.each(function(index, field) {
        var selectize = app.selectize.get($(field));
        selectize.clearOptions();
    });
};

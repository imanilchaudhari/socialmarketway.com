var path = require('path');

module.exports = {
	context: path.resolve(__dirname, 'src'),
	entry: {
		'bundle.js': [
			path.resolve(__dirname, 'src/js/site.js'),
			path.resolve(__dirname, 'src/js/app.js'),
			path.resolve(__dirname, 'src/js/review.js'),
			path.resolve(__dirname, 'src/js/selectize.js'),
			path.resolve(__dirname, 'src/js/helper.js'),
		]
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'js/[name]',
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: [
					'style-loader',
					'css-loader'
				]
			}
		]
	}
};
window.addEventListener("load", function() {
    characterCounterSeoDescription();
    characterCounterSeoForm();
    characterCounterFbTwitterAdDescription();
    characterCounterFbTwitterAdTitle();
    characterCounterImageDetailForm();
    insertHyphenOnSlug();
    CheckLastCharOfSlug();
    characterCounterImageDetailSeoDescription();
})

function characterCounterSeoDescription(){
    jQuery(".page-seo-description").each(function() {
        jQuery(this).unbind("keyup keydown change focus").bind("keyup keydown change focus", function (event) {
            var $this = jQuery(this);
            var charLength = $this.val().length;
            var messageContainer = $this.next(".char-counter");
            var maxLength = ($(this).attr("maxlength"));
            var minLength = ($(this).attr("minlength"));
            console.log(maxLength);

            if (messageContainer.length == 0) {
                $this.after("<p class=\"char-counter\"></p>");
            }
            messageContainer = $this.next(".char-counter");
            messageContainer.html( "<i style=\"color:green\">Character: " + charLength + " </i>" );

            if(charLength == maxLength) {
                messageContainer.append( "<p style=\"color:#a94442\">Title should contain at most " + maxLength + " character. Make it shorter. </p>" );
                $this.addClass('red-border');
            }else {
                $this.removeClass('red-border');
            }

        });
    });
}   

function characterCounterSeoForm(){
    jQuery(".page-title, .page-slug, .page-seo-title, .page-seo-keyword").each(function() {
        jQuery(this).unbind("keyup keydown change focus").bind("keyup keydown change focus", function (event) {
            var $this = jQuery(this);
            var charLength = $this.val().length;
            var messageContainer = $this.next(".char-counter");
            var maxLength = ($(this).attr("maxlength"));
            var minLength = ($(this).attr("minlength"));

            if (messageContainer.length == 0) {
                $this.after("<p class=\"char-counter\"></p>");
            }
            messageContainer = $this.next(".char-counter");
            messageContainer.html( "<i style=\"color:green\">Character: " + charLength + "  </i>" );

            if(charLength == maxLength) {
                messageContainer.append( "<p style=\"color:#a94442\">Title should contain at most " + maxLength + " character. Make it shorter. </p>" );
                $this.addClass('red-border');
            }else {
                $this.removeClass('red-border');
            }

        });
    });
}

function characterCounterFbTwitterAdDescription() {
    jQuery(".fb-description, .twitter-description, .ad-description").each(function() {
        jQuery(this).unbind("keyup keydown change focus").bind("keyup keydown change focus", function (event) {
            var $this = jQuery(this);
            var charLength = $this.val().length;
            var messageContainer = $this.next(".char-counter");
            var maxLength = ($(this).attr("maxlength"));
            var minLength = ($(this).attr("minlength"));

            if (messageContainer.length == 0) {
                $this.after("<p class=\"char-counter\"></p>");
            }
            messageContainer = $this.next(".char-counter");
            messageContainer.html( "<i style=\"color:green\">Character: " + charLength + "  </i>" );

            if(charLength == maxLength) {
                messageContainer.append( "<p style=\"color:#a94442\">Description should contain at most " + maxLength + " character. Make it shorter. </p>" );
                $this.addClass('red-border');
            }else {
                $this.removeClass('red-border');
            }

            if((charLength < minLength) && (charLength > '0'))  {
                messageContainer.append( "<p style=\"color:#a94442\"> Description should contain at least " + minLength + " character. Make it longer. </p>" );
                $this.addClass('red-border');

            } else {
                $this.removeClass('red-border');
            }
        });
    });
}   

function characterCounterFbTwitterAdTitle(){
    jQuery(".fb-title, .twitter-title, .ad-title").each(function() {
        jQuery(this).unbind("keyup keydown change focus").bind("keyup keydown change focus", function (event) {
            var $this = jQuery(this);
            var charLength = $this.val().length;
            var messageContainer = $this.next(".char-counter");
            var maxLength = ($(this).attr("maxlength"));
            var minLength = ($(this).attr('minlength'));

            if (messageContainer.length == 0) {
                $this.after("<p class=\"char-counter\"></p>");
            }
            messageContainer = $this.next(".char-counter");
            messageContainer.html( "<i style=\"color:green\">Character: " + charLength + "  </i>" );

            
            if(charLength == maxLength) {
                messageContainer.append( "<p style=\"color:#a94442\">Title should contain at most " + maxLength + " character. Make it shorter. </p>" );
                $this.addClass('red-border');
            }else {
                $this.removeClass('red-border');
            }

            if((charLength < minLength) && (charLength > '0'))  {
                messageContainer.append( "<p style=\"color:#a94442\"> Title should contain at least " + minLength + " character. Make it longer. </p>" );
                $this.addClass('red-border');
            } else {
                $this.removeClass('red-border');
            }

        });
    });
}

function characterCounterImageDetailForm(){
    jQuery(".img-slug, .img-seo-title, .img-alt, .img-caption").each(function() {
        jQuery(this).unbind("keyup keydown change focus").bind("keyup keydown change focus", function (event) {
            var $this = jQuery(this);

            var charLength = $this.val().length;
            var messageContainer = $this.next(".char-counter");
            if (messageContainer.length == 0) {
                $this.after("<p class=\"char-counter\"></p>");
            }
            messageContainer = $this.next(".char-counter");
            messageContainer.html( "<i style=\"color:green\">Character : " + charLength + "</i>" );


            var maxLength = ($(this).attr("maxlength"));
            var inputName = ($(this).attr('data-input'));
            var minLength = ($(this).attr("minlength"));

            if(charLength == maxLength) {
                messageContainer.append( "<p style=\"color:#a94442\"> "+ inputName + " should contain at most " + maxLength + " character. Make it shorter.  </p>" );
                $this.addClass('red-border');
            } else {
                $this.removeClass('red-border');
            }

            if((charLength < minLength) && (charLength > '0'))  {
                messageContainer.append( "<p style=\"color:#a94442\"> "+ inputName + " should contain at least " + minLength + " character. Make it Longer.</p>" );
                $this.addClass('red-border');
            } else {
                $this.removeClass('red-border');
            }
        });
    });
}

function characterCounterImageDetailSeoDescription(){
    jQuery(".img-seo-description").each(function() {
        jQuery(this).unbind("keyup keydown change focus").bind("keyup keydown change focus", function (event) {
            var $this = jQuery(this);

            var charLength = $this.val().length;
            var messageContainer = $this.next(".char-counter");
            if (messageContainer.length == 0) {
                $this.after("<p class=\"char-counter\"></p>");
            }
            messageContainer = $this.next(".char-counter");
            messageContainer.html( "<i style=\"color:green\">Character: " + charLength + "</i>" );

            var maxLength = ($(this).attr("maxlength"));
            var inputName = ($(this).attr('data-input'));
            var minLength = ($(this).attr("minlength"));

            if(charLength == maxLength) {
                messageContainer.append( "<p style=\"color:#a94442\"> "+ inputName + " should contain at most " + maxLength + " character. Make it shorter.</p>" );
                $this.addClass('red-border');
            } else {
                $this.removeClass('red-border');
            }


            if((charLength < minLength) && (charLength > '0'))  {
                messageContainer.append( "<p style=\"color:#a94442\"> "+ inputName + " should contain at least " + minLength + " character. Make it longer.</p>" );
                $this.addClass('red-border');
            } else {
                $this.removeClass('red-border');
            }
        });
    });
}

function insertHyphenOnSlug() {
    jQuery('.img-slug, .page-slug').on('input', function() {
        var slug = $(this).val();
        $(this).val(slug.replace(/\s+/g, '-'));

        var lastChar = slug[slug.length -1];
    })
}

function CheckLastCharOfSlug() {
    jQuery('.img-slug, .page-slug').on('change', function() {
        var slug = $(this).val();
        var lastChar = slug[slug.length -1];

        if(lastChar == '-') {
            this.value = slug.substr(0,slug.length-1);
        }
    })
}
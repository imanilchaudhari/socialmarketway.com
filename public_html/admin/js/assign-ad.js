let platform;
let placement;
let page_type;
let ads = [];
let placementUsedId;
$(`input[name="page_type"]`).on("change", function (e) {
    getPlacement();
});

$(`input[name="platform"]`).on("change", function (e) {
    getPlacement();
});

function getPlacement() {
    let adNode = document.getElementById("ad_table");
    adNode.innerHTML = '';
    platform = $(`input[name="platform"]:checked`).val();
    page_type = $(`input[name="page_type"]:checked`).val();
    let data = {
        platform: platform,
        page_type: page_type
    };
    $
        .ajax({
        type: "POST",
        url: Yii.app.baseUrl + 'placement/get-placement-ad',
        data: data
    })
        .done(response => {
            console.log(response);
            let placementTable =  document.getElementById("placement_table");
            placementTable.innerHTML = "";
            $.each(response.placements, function (index, value) {
                let li = document.createElement("li");
                let tr = document.createElement("tr");
                let network = value.ad? value.ad.network: "";
                let checked = value.placement.status == 1 ? 'checked':'';
                tr.innerHTML = `<td><input value=${value.placement.id} type="radio"  name="placement" /> <label>${value.placement.name}</label></td>
                <td>${network}</td>
                <td><input value=${value.placement.id} type="checkbox" class="js-switch placement"  name="placement_status" ${checked}/></td>`
                placementTable.appendChild(tr);
            });
            switchery('placement');
            
        })
}



$(document)
    .on("click", function (e) {
        
        if($(e.target).attr('name') == "placement_status") {
            let input = $(e.target).prev();
            let id = $(input).val();
            $.ajax({
                type:"POST",
                url:Yii.app.baseUrl + 'placement/change-placement-status',
                data:{placement:id, status:$(input).is(':checked') ? 1:0}
            });
        }

        if (e.target.name == "placement") {
            placement = e.target.value;

            let data = {
                placement: e.target.value,
                page_type: page_type,
                platform: platform,
                placement_type: "external"
            };

            $
                .ajax({
                type: "POST",
                url: Yii.app.baseUrl + 'placement/get-ad',
                data: data
            })
                .done(response => {
                    ads = [];
                    let adNode = document.getElementById("ad_table");
                    if (response.placementUsed) {
                        placementUsedId = response.placementUsed.id;
                        adNode.innerHTML = "";
                        $.each(response.ads, function (index, value) {

                            let tr = document.createElement("tr");
                            let checked = value.placement_id == null
                                ? ""
                                : "checked";
                            checked
                                ? ads.push(value.id)
                                : "";
                            let assignedAd = "";

                            if (checked) {
                                let radioAd = value.id == response.placementUsed.ad_id
                                    ? "checked"
                                    : "";
                                assignedAd = `<input value=${value.id} type="radio"   id="assignedAd"  name="assignedAd" ${radioAd}/>`;
                            }

                            tr.innerHTML = `<td><input value=${value.id} type="checkbox" class="js-switch ad"  name="ad" ${checked}/></td><td>${value.network}</td><td><label>${value.name}</label></td><td>${assignedAd}</td>`;
                            adNode.appendChild(tr);
                        });
                        switchery('ad');
                    } else {
                        adNode.innerHTML = '';
                    }

                })
        }
        let ele = $(e.target)[0].tagName == 'SMALL'
            ? $(e.target)
                .parent()
                .prev()
            : $(e.target).prev();

        if ($(ele).attr('name') == "ad") {
            let value = $(ele).attr('value');
            let newads = [];
            status = ads.includes(parseInt(value))
                ? 0
                : 1;
            platform = $(`input[name="platform"]:checked`).val();
            page_type = $(`input[name="page_type"]:checked`).val();
            let data = {
                placement: placement,
                ads: value,
                status: status,
                platform: platform,
                page_type: page_type,
                placement_type: 'external'
            };
            $
                .ajax({
                type: "POST",
                url: Yii.app.baseUrl + 'placement/assign-ad',
                data: data
            })
                .done(response => {
                    console.log(status);
                    if (!$(ele).is(':checked')) {
                        $(`input[type='radio'][value="${value}"]`).remove();
                        // let count = ads.indexOf(parseInt(value)); console.log(count);
                        // ads.splice(count, count + 1); console.log(ads);
                    } else {

                        let sibling = $(ele)
                            .parent()
                            .next()
                            .next()
                            .next();
                        console.log(sibling);
                        $(sibling).append('');
                        $(sibling).append(`<input value=${value} type="radio"   id="assignedAd"  name="assignedAd"/>`);
                        // ads.push(parseInt(e.target.value)); console.log(ads);

                    }

                })
        }

        if (e.target.name == "assignedAd") {
            platform = $(`input[name="platform"]:checked`).val();
            page_type = $(`input[name="page_type"]:checked`).val();
            let data = {
                page_type: page_type,
                placementUsedId: placementUsedId,
                ad_id: e.target.value,
                platform: platform,
                placement_id: placement
            };
            $
                .ajax({
                type: "POST",
                url: Yii.app.baseUrl + 'placement/placement-ad',
                data: data
            })
                .done(response => {
                    console.log(response);
                })
        }

    });

function switchery(type) {
    $(`.js-switch.${type}`)
        .each(function () {
            new Switchery(this);
        });
}
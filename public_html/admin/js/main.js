(function ($) {
    'use strict';

    $('body').on('beforeValidate', 'form.disable-submit-buttons', function (e) {
        $(':input[type="submit"]', this).attr('disabled', 'disabled');
        $(':input[type="submit"][data-disabled-text]', this).each(function (i) {
            var $this = $(this)
            if ($this.prop('tagName') === 'input') {
                $this.data('enabled-text', $this.val());
                $this.val($this.data('disabled-text'));
            } else {
                $this.data('enabled-text', $this.html());
                $this.html($this.data('disabled-text'));
            }
        });
    }).on('afterValidate', 'form.disable-submit-buttons', function (e) {
        if ($(this).find('.has-error').length > 0) {
            $(':input[type="submit"]', this).removeAttr('disabled');
            $(':input[type="submit"][data-disabled-text]', this).each(function (i) {
                var $this = $(this)
                if ($this.prop('tagName') === 'input') {
                    $this.val($this.data('enabled-text'));
                } else {
                    $this.html($this.data('enabled-text'));
                }
            });
        }
    });

    $.fn.slugify = function(text) {
        var slug = text.toLowerCase().replace(/[^\w ]+/g,"").replace(/ +/g,"-");
        return slug;
    }

    $.fn.getTitleWidth = function(obj){
        var text = this.slugify(obj.val());
        var placeholder = $(".snippet-slug");
        placeholder.html("https://ecelebrityfacts.com/" + text);
    };

    $.fn.getSeoSlugWidth = function(obj){
        var text = obj.val();
        var placeholder = $(".snippet-slug");
        placeholder.html("https://ecelebrityfacts.com/" +text);
    };

    $.fn.getSeoTitleWidth = function(obj) {
        var text = obj.val();
        // var google_css = 'color: #12C;cursor: pointer;display: inline;font-family: arial, sans-serif;font-size: 16px;font-weight: normal;height: auto;line-height: 19px;list-style-image: none;list-style-position: outside;list-style-type: none;margin-bottom: 0px;margin-left: 0px;margin-right: 0px;margin-top: 0px;overflow-x: visible;overflow-y: visible;padding-bottom: 0px;padding-left: 0px;padding-right: 0px;padding-top: 0px;text-align: -webkit-auto;text-decoration: underline;text-overflow: clip;visibility: visible;white-space: nowrap;width: auto;';
        //    google_css = google_css.split(";");

        // make placeholder
        var placeholder = $(".snippet-title");

        // make new obj
        var newObj = $("<span/>"); newObj.html(text);

        // add style
        // for(var i=0; i<google_css.length; i++) {
        //     var c = google_css[i].split(":");
        //     if( c[0]!="" && c[1]!="" ){ newObj.css(c[0],c[1]); }
        // }

        placeholder.html( newObj );
        
        // var width = newObj.width();


        // if (width > 496) {
        //     var charPosWhereWidthLimitIsCrossed;
        //     for (var i = text.length; i > 0; i--) {
        //         var testObj = $("<span/>");
        //         testObj.html(text.substring(0, i));
        //         for (var j = 0; j < google_css.length; j++) {
        //             var c = google_css[j].split(":");
        //             if (c[0] != "" && c[1] != "") { testObj.css(c[0],c[1]); }
        //         }

        //         placeholder.html( testObj );
        //         if (testObj.width() <= 496) {
        //             charPosWhereWidthLimitIsCrossed = i;
        //             break;
        //         }
        //     }

        //     var shortText = text.substring(0, charPosWhereWidthLimitIsCrossed) + "...";

        //     placeholder.empty().html(shortText);
        // } else {
        //     placeholder.html(newObj)
        // }
    };
}(jQuery));

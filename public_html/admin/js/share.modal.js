(function ($) {
    'use strict';

    var ShareModal = function (button, options) {
        this.button = $(button);

        this.settings = $.extend({
            url: 'index.php?share/index',
            title: 'Share Modal'
        }, options);

        var self = this;

        this.actionRenderModal = function () {
            if ($('body').find('#share-browser-modal').length === 0) {
                var modalHeader = '<div class="modal-header">'
                    + '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'
                    + '<span aria-hidden="true">&times;</span>'
                    + '</button>'
                    + '<h4 class="modal-title">' + this.settings.title + '</h4>'
                    + '</div>';
                var modalBody = '<div class="modal-body"></div>';
                var modalContent = '<div class="modal-content">'
                    + modalHeader
                    + modalBody
                    + '</div>';
                var modalDialog = '<div class="modal-dialog" role="document">'
                    + modalContent
                    + '</div>';
                var modal = '<div class="share-browser-modal has-footer modal fade" id="share-browser-modal" tabindex="-1" role="dialog" aria-labelledby="share-browser-modal">'
                    + modalDialog
                    + '</div>';
                jQuery('body').append(modal);
            }
        };

        this.actionShowModal = function () {
            this.button.on('click', function (e) {
                e.preventDefault();
                var $modal = $('body').find('#share-browser-modal');

                $modal.find('.modal-body').html('<iframe src="' + self.settings.url + '"></iframe>');
                $modal.modal('show');
            });
        };

        this.actionHideModal = function () {
            $(document).on('hide.bs.modal', '#share-browser-modal', function(){
                var $this = $(this);

                $this.find('.modal-body').html('');
            });
        };

        this.actionInit = function () {
            this.actionRenderModal();
            this.actionShowModal();
            this.actionHideModal();
        };

        return this.actionInit();
    };

    $.fn.sharemodal = function (options) {
        return this.each(function() {
            var element = $(this);

            if (element.data('sharemodal')) {
                return;
            }

            var sharemodal = new ShareModal(this, options);

            element.data('sharemodal', sharemodal);
        });
    }
}(jQuery));

document.addEventListener("DOMContentLoaded", function() {

    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        return elementBottom > viewportTop && elementTop < viewportBottom;
    };
    $(window).scroll(function() {
        var referenceArticleBlock = $('.reference-article');
        var height = $(window).scrollTop();
        if (height > 110) {
            referenceArticleBlock.css('top', '10px');
            referenceArticleBlock.css('padding-bottom', '10px');
            referenceArticleBlock.css('height', 'calc( 90vh - 10px )');
        } else {
            var diff = 220 - height;
            referenceArticleBlock.css('top', diff + 'px');
            referenceArticleBlock.css('height', 'calc( 95vh - ' + diff + 'px )');
        }
    });

    $('.user-guideline-btn').click(function(e) {
        e.preventDefault();
        var guidelineModal = $('#userGuidelineModal');

        if ($(this).hasClass('seo-guideline')) {
            $('.seo-introduction').css('display', 'block');
            $('#userGuidelineModal .share-card ').hide();
            $('#userGuidelineModal .ad-campaign ').hide();
        } else if ($(this).hasClass('share-card-guideline')) {
            $('#userGuidelineModal .share-card ').css('display', 'block');
            $('.seo-introduction').hide();
            $('#userGuidelineModal .ad-campaign ').hide();
        } else {
            $('#userGuidelineModal .ad-campaign ').css('display', 'block');
            $('.seo-introduction').hide();
            $('#userGuidelineModal .share-card ').hide();

        }

        guidelineModal.modal('show');
    })

    $('.fb-example-btn').click(function(e) {
        e.preventDefault();
        var exampleModal = $('#fbExampleModal');
        exampleModal.modal('show');
    })

    $('.twitter-example-btn').click(function(e) {
        e.preventDefault();
        var exampleModal = $('#twitterExampleModal');
        exampleModal.modal('show');
    })

    $('.ad-campaign-example-btn').click(function(e) {
        e.preventDefault();
        var exampleModal = $('#adExampleModal');
        exampleModal.modal('show');
    })

    $('.version-card').each(function() {
        var inputContainer = $(this);
        var urlInputArea = $('.img-url-og', inputContainer);
        var titleInputArea = $('.title-og', inputContainer);
        var descriptionArea = $('.description-og', inputContainer);
        var imageDisplayed = $('.preview-image', inputContainer);
        var titleDisplayed = $('.share-title',inputContainer);
        var descriptionDisplayed = $('.share-description',inputContainer);

        // setInterval(function(){
        //     var currentValue = urlInputArea.val();
        //     $(imageDisplayed).attr('src', currentValue);
        // },1000);


        titleInputArea.on('input',function(){
            $('.title-and-description ', inputContainer).show();
            var currentTitle = titleInputArea.val();
            $(titleDisplayed).text(currentTitle);
        });

        descriptionArea.on('input',function(){
            var currentDescription = descriptionArea.val();
            $(descriptionDisplayed).text(currentDescription);
        });
    });

    $('.img-detail-example-btn').click(function(e){
        e.preventDefault();
        var imageDetailExampleModal = $('#imageDetailExampleModal');
        imageDetailExampleModal.show();
    })

    $('#imageDetailExampleModal .close-button, .go-back-btn').click(function(e){
        e.preventDefault();
        $('#imageDetailExampleModal').hide();

    })
});
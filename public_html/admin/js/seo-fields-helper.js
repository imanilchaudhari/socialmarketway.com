(function seoFieldsHelper($) {
	$(".title-content-seo")
		.each(function () {
			$(this)
				.unbind("keyup keydown change focus")
				.bind("keyup keydown change focus", function (event) {
					var charLength = $("input", this)
						.val()
						.length;
					var messageContainer = $(".help-block", this);
					messageContainer.html("<i style=\"color:green\">Character Count : " + charLength + "</i>");

					var charLimit = parseInt(69);
					if (charLength > charLimit) {
						messageContainer.html("<i style=\"color:red\">Character Count : " + charLength + " Please try to keep character-length below " + charLimit + " characters if possible.</i>");
					}
				});
		});

	$(".seo-description").each(function () {
		$(this)
			.unbind("keyup keydown change focus")
			.bind("keyup keydown change focus", function (event) {
				var charLength = $(this)
					.val()
					.length;
				var messageContainer = $(this).next(".char-counter");
				if (messageContainer.length == 0) {
					$(this).after("<p class=\"char-counter\"></p>");
				}
				messageContainer = $(this).next(".char-counter");
				messageContainer.html("<i style=\"color:green\">Character Count : " + charLength + "</i>");

				var charLimit = parseInt(140);
				if (charLength > charLimit) {
					messageContainer.html("<i style=\"color:red\">Character Count : " + charLength + " Please try to keep character-length below " + charLimit + " characters if possible.</i>");
				}
			});
	});

	$(".title-content")
		.unbind("change keyup select")
		.bind("change keyup select", function (ev) {
			ev.preventDefault();
			var slug = convertToSlug($(this).val().trim());
			$(".slug-content").val(slug);
		});
})(jQuery);

function getEditForm(id) {
	$.ajax({
		type: "GET",
		url: Yii.app.baseUrl + "ajax/get-edit-form",
		data: {
			id: id
		},
		dataType: "HTML"
	})
	.done(function (response) {
		jQuery('#page-container-' + id).html(response);
	});
}

function approveSection(id, caller) {
	sectionBulkAction(id, caller, "approve");
}

function rejectSection(id, caller) {
	sectionBulkAction(id, caller, "reject");
}

function sectionBulkAction(id, caller, action) {
	var loading = makeLoadingAnimation(caller);

	loading.show();
	jQuery
		.ajax({
			type: "POST",
			url: Yii.app.baseUrl + "seo-to-update/bulk-action",
			data: {
				ids: id,
				action: action,
				content: "section",
				_csrf: yii.getCsrfToken()
			}
		})
		.done(function (response) {
			$
				.pjax
				.reload({
					container: "#pjax-page-" + id
				});
			toastr.success("Rejected successfully.");
		})
		.fail(function (response) {
			window
				.location
				.reload();
			toastr.error("Oops! error occured.");
		})
		.always(loading.hide());
}

function addPage(parent_id, caller) {
	if (window.confirm("Add new page?")) {
		var loading = makeLoadingAnimation(caller);
		loading.show();

		jQuery.ajax({
			type: "GET",
			url: Yii.app.baseUrl + "seo-to-update/add-new-page",
			data: {
				parent_id: parent_id,
				_csrf: yii.getCsrfToken()
			},
				dataType: "JSON"
			})
			.done(function (response) {
				if (response.status) {
					$("[id^=pjax-page-]")
						.last()
						.append(response.html);
				} else {
					toastr.error(response.message);
				}
			})
			.always(loading.hide());
	}
}

function deleteSection(id, caller) {
	if (window.confirm("Are you sure?")) {
		var loading = makeLoadingAnimation(caller);
		loading.show();

		jQuery.ajax({
			type: "POST",
			url: Yii.app.baseUrl + 'ajax/delete-page?id=' + id,
			data: {
				action: "delete",
				content: "section",
				type: "single",
				_csrf: yii.getCsrfToken()
			},
				dataType: "JSON"
			})
			.done(function (response) {
				if (response.status == true) {
					toastr.success(response.message);
					$("#pjax-page-" + id).remove();
				} else {
					toastr.error(response.message);
				}
			})
			.always(loading.hide());
	}
}

function makeLoadingAnimation(caller) {
	return {
		element: $(caller),
		elementHtml: $(caller).html(),
		show: function () {
			$(this.element).html('<i class="fa fa-refresh fa-spin"></i>');
		},
		hide: function () {
			var _this = this;
			window.setTimeout(function () {
				$(_this.element).html(_this.elementHtml)
			}, 200);
		}
	};
}

function togglePageSeoForm(page_id, container) {
	var url = Yii.app.baseUrl + 'page/get-page-seo-form';
	togglePageForm(page_id, url, container);
}

function togglePageForm(page_id, url, container) {
	var form_container = container
		? $(container)
		: $(".page-image-seo-container-" + page_id);
	if (hasChildren(form_container)) {
		removeChildren(form_container);
	} else {
		$
			.get(url, {
				page_id: page_id
			}, function (response) {
				form_container.html(response);
				seoFieldsHelper();
				form_container
					.children()
					.show();
			});
	}
}

function hasChildren(element) {
	return element
		.children()
		.length;
}

function removeChildren(element) {
	element
		.children()
		.remove();
}

// /////////////////////////////////////////////////////////////////////// ///
// Functions for Edit form                                ////////
// //////////////////////////////////////////////////////////////////////
function getContentFromEditor(page_id) {
	return remove_emojies($("textarea", "#section-edit-form-" + page_id).val());
}

function saveSectionContent(page_id) {
	jQuery.ajax({
		method: "POST",
		url: Yii.app.baseUrl + 'ajax/save-page?id=' + page_id,
		data: {
			detail: getContentFromEditor(page_id),
			_csrf: yii.getCsrfToken()
		}
	}).done(notifyWhenContentIsSaved(page_id));
}

function notifyWhenContentIsSaved(page_id) {
	var info_container = $('.counter-' + page_id);
	var html = '<mark class="saved-notification-' + page_id + '">Saved</mark>';
	info_container.append(html);

	setTimeout(function () {
		$('.saved-notification-' + page_id).remove();
	}, 2345);
}

function removeSectionContentEditor(page_id) {
	var content = getContentFromEditor(page_id);
	$("#page-container-" + page_id).html(content);
}

function convertToSlug(text) {
	var slug = text
		.toLowerCase()
		.replace(/[^\w ]+/g, "")
		.replace(/ +/g, "-");
	return slug;
}
(function ($) {
    'use strict';

    function ImageBrowser(browser, options) {
        this.document = $(document);
        this.browser = $(browser);
        this.upload = this.browser.find('.image-upload');
        this.dropzone = this.upload.find('.dropzone');
        this.container = this.browser.find('.image-container');
        this.details = this.browser.find('.image-details');
        this.form = this.browser.find('.image-form');
        this.filter = this.browser.find('.image-filter');
        this.items = {
            files: []
        };
        this.selected = {};
        this.settings = $.extend({
            templates: {
                download: 'template-download',
                upload: 'template-upload',
                details: 'template-details',
                form: 'template-form',
                item: 'template-item'
            },
            url: {
                upload: 'index.php?r=image/ajax-upload',
                download: 'index.php?r=image/download',
                assign: 'index.php?r=image/assign',
                swap: 'index.php?r=image/swap',
                edit: 'index.php?r=image/edit',
                update: 'index.php?r=image/ajax-update',
                json: 'index.php?r=image-browser/get-json',
                insert: 'index.php?r=image-browser/field-insert',
                form: 'index.php?r=image/get-form',
                quick_insert : 'index.php?r=image-browser/quick-editor-insert'
            },
            editor: false,
            multiple: false,
            callback: '',
            autoload: true
        }, options);
        this.lockGetData = false;

        if (this.settings.callback !== '') {
            this.callback = this.settings.callback;
        }

        var self = this;

        this.actionGetData = function (data) {
            if (this.lockGetData === false) {
                var url = this.settings.url.json
                console.log(url);
                // let other = $('#dataentry').val();
                // let dataentry_id = $('input[name="dataentry_id"]').val();
                // console.log( this.filter.serializeArray());
               // other = dataentry_id == other ? null : other;

                $.ajax({
                    url: url,
                    data: this.filter.serializeArray(),
                    dataType: 'JSON'
                })
                .done(function (response) {
                    self.items = response;
                    self.container.html(tmpl(self.settings.templates.download, response));
                    self.lockGetData = false;
                })
                .fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    toastr.error(XMLHttpRequest.responseText);
                });
            }
        };

        this.actionScroll = function () {
            var containerParent = self.container.parent();

            if ((this.container.height() <= containerParent.height())) {
                this.actionGetData(this.filter.serializeArray(), true);
                this.lockGetData = true;
            }

            containerParent.scroll(function () {
                var $this = $(this);

                if ((($this.height() * (3 / 2)) >= ((this).scrollHeight - $this.scrollTop) )) {
                    self.actionGetData(self.filter.serializeArray(), true);
                    self.lockGetData = true;
                }
            })
        };

        this.actionUpload = function () {
            this.upload.fileupload({
                    url: this.settings.url.upload,
                    dropZone: this.dropzone,
                    autoUpload: true,
                    filesContainer: this.container,
                    prependFiles: true
                })
                .fileupload("option", "redirect", window.location.href.replace(/\/[^\/]*$/, "/cors/result.html?%s"))
                .addClass('fileupload-processing');

            this.document.bind('dragover', function (e) {
                var found = false,
                    node = e.target,
                    foundDropzone,
                    timeout = window.dropZoneTimeout;

                if (!timeout) {
                    self.dropzone.addClass('in');
                } else {
                    clearTimeout(timeout);
                }

                do {
                    if ($(node).hasClass('dropzone')) {
                        found = true;
                        foundDropzone = $(node);
                        break
                    }
                    node = node.parentNode;
                } while (node !== null);

                self.dropzone.removeClass('in hover');

                if (found) {
                    foundDropzone.addClass('hover');
                }

                window.dropZoneTimeout = setTimeout(function () {
                    window.dropZoneTimeout = null;
                    self.dropzone.removeClass('in hover');
                }, 100);
            });

            self.upload.bind('fileuploaddone', function (e, data) {
                $.each(data.result, function (i, file) {
                    self.items.files[self.items.files.length] = file[0];
                });
            });

            self.upload.bind('fileuploadstart', function () {
                self.browser.find('a[href="#image-library"]').click();
            });
        };

        this.actionRemoveItem = function (id) {
            // this.details.html('');
            // this.form.html('');
            // $("#image-form-container").html("");
            delete this.selected[id];
            var selectedIndex = Object.keys(this.selected);

            if (selectedIndex.length > 0) {
                var index = parseInt(selectedIndex[selectedIndex.length - 1]);
                $.each(this.items.files, function (i, file) {
                    if (file.id === index) {
                        self.details.html(tmpl(self.settings.templates.details, file));
                        self.form.html(tmpl(self.settings.templates.form, file));

                        return false
                    }
                });
            } else {
                this.browser.removeClass('visible-lr');
            }
        };

        this.actionAddItem = function (id, ui) {
            $.each(this.items.files, function (i, file) {
                if (id === file.id) {
                    self.details.html(tmpl(self.settings.templates.details, file));
                    file.for_editor = !!self.settings.editor;
                    self.form.html(tmpl(self.settings.templates.form, file));

                    if (!self.settings.multiple || self.settings.multiple === 'false') {
                        $(ui.selected).addClass('ui-selected').siblings().removeClass('ui-selected').each(
                            function (key, value) {
                                $(value).find('*').removeClass('ui-selected');
                            }
                        );
                        self.selected = {};
                    }

                    self.selected[id] = self.browser.find('.image-form-inner').first().serializeArray();
                    self.browser.addClass('visible-lr');

                    if (Object.keys(self.selected).length === 1) {
                        window.location.hash = 'image-' + id;
                    }

                    return false;
                }
            });
        };

        this.actionSelectItem = function () {
            this.container.on('touchstart mousedown', 'li', function (event, ui) {
                event.metaKey = true;
                var $this = $(this);
                var id = $this.data("id");
                var dataentry_id = $this.data("dataentry-id");
                $("#image-form-container").html("");
                $.ajax({
                    url: self.settings.url.form,
                    data: { id: id, dataentry_id: dataentry_id, _csrf: yii.getCsrfToken() },
                    type: 'GET'
                }).done(function (response) {
                    $("#image-form-container").html(response);

                    // var src = $("img", $this).prop("src");
                    // var $html = '<img src="' + src + '" class="img img-responsive" />';
                    // $html = $html + response;
                    // var $modal = $("body").find("#image-edit-modal");
                    // $modal.find(".modal-body").html($html);
                    // $modal.modal("show");
                }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
                    toastr.error(XMLHttpRequest.responseText);
                });

                if ($this.hasClass('ui-selected')) {
                    $this.removeClass('ui-selected');
                    self.actionRemoveItem($this.data('id'));
                    return false;
                }
            }).selectable({
                filter: 'li',
                tolerance: 'touch',
                selected: function (event, ui) {
                    var id = $(ui.selected).data('id');
                    self.actionAddItem(id, ui);
                },
                unselected: function (event, ui) {
                    var id = $(ui.unselected).data('id');
                    self.actionRemoveItem(id);
                }
            });
        };

        this.actionDownload = function (data) {
            self.browser.on('click', '.download-image', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var $this = $(this);

                $.ajax({
                    url: $this.data('url'),
                    type: "GET",
                    success: function () {

                    }
                });
            });
        };

        this.actionAssign = function (data) {
            self.browser.on('click', '.assign-image', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var $this = $(this);

                if (confirm($this.data('confirm'))) {
                    $.ajax({
                        url: $this.data('url'),
                        type: "GET"
                    })
                    .done(function (response) {
                        self.actionGetData(self.filter.serializeArray());
                    })
                    .fail(function (XMLHttpRequest, textStatus, errorThrown) {
                        toastr.error(XMLHttpRequest.responseText);
                    })
                    .always(function (data) {
                        return;
                    });
                }
            });
        };

        this.actionSwap = function (data) {
            self.browser.on('click', '.swap-image', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var $this = $(this);

                if (confirm($this.data('confirm'))) {
                    $.get($this.data('url'))
                    .done(function (response) {
                        self.actionGetData(self.filter.serializeArray());
                    })
                    .fail(function (XMLHttpRequest, textStatus, errorThrown) {
                        toastr.error(XMLHttpRequest.responseText);
                    });
                }
            });
        };

        this.actionEdit = function (data) {
            self.browser.on('click', '.edit-image', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var $this = $(this);

                $.ajax({
                    url: $this.data('url'),
                    type: "GET",
                    success: function () {

                    }
                });
            });
        };

        this.actionUpdateItem = function () {
            this.browser.on('blur', '.image-form-inner [id^="image-"]', function () {
                var $this = $(this),
                    parent = $this.parents('.image-form-inner'),
                    id = parent.data('id');

                self.selected[id] = parent.serializeArray();
            })
        };

        this.actionDeleteItem = function () {
            self.browser.on('click', '.delete-image', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var $this = $(this);

                if (confirm($this.data('confirm'))) {
                    $.ajax({
                        url: $this.data('url'),
                        type: "DELETE",
                        success: function (response) {
                            if (typeof response.status !== "undefined" && response.status == false) {
                                alert(response.message);
                                return;
                            } else {
                                self.container.find('.image-item[data-id="' + $this.data('id') + '"]').remove();
                                self.details.html('');
                                self.form.html('');
                                $("#image-form-container").html("");
                                delete self.selected[$this.data('id')];
                            }
                        }
                    });
                }
            });
        };

        this.actionUpdateAttribute = function () {
            self.browser.on('blur', '.image-form-inner [data-ajax-update]', function () {
                var $this = $(this),
                    parent = $this.parents('.image-form-inner');

                $.ajax({
                    url: self.settings.url.update,
                    type: "POST",
                    dataType: 'json',
                    data: {
                        id: parent.data('id'),
                        attribute: $this.data('ajax-update'),
                        value: $this.val(),
                        _csrf: yii.getCsrfToken()
                    }
                });
            });
        };

        this.actionChangeLink = function () {
            self.browser.on('change', '#image-link-to', function () {
                var $this = $(this),
                    value = self.browser.find('#image-link-value');

                if ($this.val() === 'none') {
                    value.val('');
                    value.attr('readonly', true);
                } else if ($this.val() === 'custom') {
                    value.val('http://');
                    value.attr('readonly', false);
                } else {
                    value.val($this.val());
                    value.attr('readonly', true);
                }
            });
        };

        this.actionFilter = function () {
            this.filter.on('submit change', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var $this = $(this);
                self.selected = {};
                self.actionGetData($this.serializeArray());
            });
        };

        this.actionInsert = function () {
            self.browser.on('click', '.insert-image', function (e) {
                e.preventDefault();
                console.log('insert');
                if (Object.keys(self.selected).length === 0) {
                    return false;
                }

                $.ajax({
                    url: self.settings.url.insert,
                    data: {Image: self.selected, _csrf: yii.getCsrfToken()},
                    type: 'POST',
                    success: function (response) {

                        console.log(response);


                        if (top.tinymce !== undefined && self.settings.editor) {
                            top.tinymce.activeEditor.execCommand("mceInsertContent", false, response);
                            window.top.$('#image-browser-modal').modal('hide');
                        }

                        if (window.top) {
                            if (self.callback && typeof window['top'][self.callback](response) === 'function') {
                                window['top'][self.callback](response);
                            }
                            window.top.$('#image-browser-modal').modal('hide');
                        }
                    }
                });
            });
        };

        this.actionQuickInsert = function () {
            self.browser.on('click', '.quick-insert-image', function (e) {
                e.preventDefault();

                var object = $(this);
                var page_id = object.data("page-id");

                if (Object.keys(self.selected).length === 0) {
                    return false;
                }
                var image_id = Object.keys(self.selected)[0];
                $.getJSON({
                    url: self.settings.url.quick_insert,
                    data: { image_id: image_id, _csrf: yii.getCsrfToken() },
                    success: function (response) {

                        if(typeof(response.success) != "undefined" && response.success !== true) {
                            toastr.warning(response.message);                          
                            return false;
                        }

                        var parent$ = window.parent.$;
                        var summernoteContainer = parent$('.edit-form-' + page_id);

                        if (summernoteContainer) {
                            var range = summernoteContainer.summernote('createRange');

                            summernoteContainer.summernote('saveRange');
                            summernoteContainer.summernote('restoreRange');
                            summernoteContainer.summernote('focus');
                            summernoteContainer.summernote('insertImage', response.url, function ($image) {
                                console.log($image);
                                //$image.css('width', $image.width() / 4);
                                $image.css('width', '25%');
                                $image.attr('data-id', response.id);
                                $image.attr('data-dataentry_id', response.dataentry_id);
                                $image.attr('data-filename', response.image);
                                $image.attr('alt', response.alt_name);
                                $image.attr('data-image_caption', response.image_caption);
                                $image.attr('data-image_source', response.image_source);
                                $image.attr('title', response.image_caption);
                            });
                            // summernoteContainer.summernote('insertText', 'Caption : ' + response.image_caption);
                            // summernoteContainer.summernote('insertText', 'Source : ' +response.image_source);

                            window.top.$('#image-browser-modal').modal('hide');
                        } else {
                            toastr.error("Invalid container.");
                        }
                    }
                });
            });
        };

        this.actionInit = function () {
            this.actionGetData(this.filter.serializeArray());
            this.actionScroll();
            this.actionUpload();
            this.actionSelectItem();
            this.actionUpdateItem();
            this.actionDeleteItem();
            this.actionUpdateAttribute();
            this.actionChangeLink();
            this.actionFilter();
            this.actionInsert();
            this.actionDownload();
            this.actionAssign();
            this.actionSwap();
            this.actionQuickInsert();
        };

        return this.actionInit();
    }

    $.fn.imagebrowser = function (options) {
        return this.each(function () {
            var $this = $(this);

            if ($this.data('imagebrowser')) {
                return;
            }

            var imagebrowser = new ImageBrowser(this, options);

            $this.data('imagebrowser', imagebrowser);
        });
    };
}(jQuery));

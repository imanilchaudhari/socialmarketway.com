$(document).ready(function() {

    var owl = $("#demo2 #owl-demo");

    owl.owlCarousel({

      // Define custom and unlimited items depending from the width
      // If this option is set, itemsDeskop, itemsDesktopSmall, itemsTablet, itemsMobile etc. are disabled
      // For better preview, order the arrays by screen size, but it's not mandatory
      // Don't forget to include the lowest available screen size, otherwise it will take the default one for screens lower than lowest available.
      // In the example there is dimension with 0 with which cover screens between 0 and 450px
      
      itemsCustom : [
        [0, 1],
        [450, 1],
        [600, 1],
        [700, 1],
        [1000, 2],
        [1200, 2],
        [1400, 2],
        [1600, 2]
      ],
      navigation : true,
      navigationText: [
        "<i class='icon-chevron-left'><img src='images/client-arrow-lft.png' alt=''/></i>",
        "<i class='icon-chevron-right'><img src='images/client-arrow-rht.png' alt=''/></i>"
        ], 

    });



  });
  $(document).ready(function() {
      $('body').find(".panel.panel-default").click(function(){
      
      
        var src_img=$(this).find(".preview").attr("src");
        $("#changeMe1").attr('src',src_img);
      })
    var owl = $(".banner-section #owl-demo");

    owl.owlCarousel({
      navigation : true,
      pagination: true,
      singleItem : true,
      autoplay: true,
      autoPlaySpeed: 1000,
      slideSpeed : 2000,
      loop: true,
      navigationText: [
        "<i class='icon-chevron-left'><img src='images/banner-arrow-lft.png' alt=''/></i>",
        "<i class='icon-chevron-right'><img src='images/banner-arrow-rht.png' alt=''/></i>"
        ],
      transitionStyle : "fade"
    });
    

    //Select Transtion Type
    $("#transitionType").change(function(){
      var newValue = $(this).val();

      //TransitionTypes is owlCarousel inner method.
      owl.data("owlCarousel").transitionTypes(newValue);

      //After change type go to next slide
      owl.trigger("owl.next");
    });
  });
  
    
   $( 'ul.nav.nav-tabs  a' ).click( function ( e ) {
      e.preventDefault();
      $( this ).tab( 'show' );
    } );

    ( function( $ ) {
        // Test for making sure event are maintained
        $( '.js-alert-test' ).click( function () {
          alert( 'Button Clicked: Event was maintained' );
        } );
        fakewaffle.responsiveTabs( [ 'xs', 'sm' ] );
    } )( jQuery );
    
    
    
    
    
    $(document).ready(function() {

$(".toggle-accordion").on("click", function() {
  var accordionId = $(this).attr("accordion-id"),
    numPanelOpen = $(accordionId + ' .collapse.in').length;
  
  $(this).toggleClass("active");

  if (numPanelOpen == 0) {
    openAllPanels(accordionId);
  } else {
    closeAllPanels(accordionId);
  }
})

openAllPanels = function(aId) {
  console.log("setAllPanelOpen");
  $(aId + ' .panel-collapse:not(".in")').collapse('show');
}
closeAllPanels = function(aId) {
  console.log("setAllPanelclose");
  $(aId + ' .panel-collapse.in').collapse('hide');
}
   
});

if ($(window).width() > 991){
  
jQuery('a[data-toggle="collapse"]').on('click',function(){
jQuery('.panel').removeClass('active');

if(jQuery(this).attr('aria-expanded')!= "true"){
    jQuery(this).closest('.panel').addClass('active');
}
});
}else
{
  jQuery('#collapse-myTab > .panel-default > .panel-heading a[data-toggle="collapse"]').on('click',function(){
      jQuery(this).attr('aria-expanded',false);
var me =jQuery(this);
jQuery("#collapse-myTab > .panel-default > .panel-collapse").each(function(){
      jQuery(this).removeClass('in');
        
      //		jQuery(this).removeClass('my_class');
      })
me.find('#collapse-myTab > .panel-default > .panel-collapse').addClass('in');
});
  
  }
  
   
  
if ($(window).width() < 991){
jQuery('.paneldefault a[data-toggle="collapse"]').on('click',function(){
jQuery('.panel').removeClass('active');
if(jQuery(this).attr('aria-expanded')!= "true"){
    jQuery(this).closest('.paneldefault').addClass('active');

    

}
});
}
$(window).scroll(function() {    
  var scroll = $(window).scrollTop();

   //>=, not <=
  if (scroll >= 100) {
      //clearHeader, not clearheader - caps H
      $("nav.navbar.navbar-default").addClass("sticky");
  }
  else  {
      $("nav.navbar.navbar-default").removeClass("sticky");
  }
}); //missing );


$('.go-top').click(function(event) {
  event.preventDefault();
  $('html, body').animate({scrollTop: 0}, 1100);
});


// handle links with @href started with '#' only
$(document).on('click', '.navbar a[href^="#"]', function(e) {
  // target element id
  var id = $(this).attr('href');

  // target element
  var $id = $(id);
  if ($id.length === 0) {
      return;
  }

  // prevent standard hash navigation (avoid blinking in IE)
  e.preventDefault();

  // top position relative to the document
  var pos = $id.offset().top;

  // animated top scrolling
  $('body, html').animate({scrollTop: pos});
});
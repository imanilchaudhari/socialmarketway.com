<?php
/**
 * @link      http://www.anilchaudhari.com.np/
 * @author    Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license   http://www.anilchaudhari.com.np/license/
 */

return [
    'name' => 'toolbar',
    'title' => 'Toolbar',
    'config' => [
        'frontend' => [
            'class' => 'modules\toolbar\frontend\Module',
        ],
    ],
    'frontend_bootstrap' => 1,
];

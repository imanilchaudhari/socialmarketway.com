<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

namespace widgets\meta;

use frontend\components\BaseWidget;
use Yii;
use yii\widgets\Menu;

/**
 * Class MetaWidget
 *
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @since 0.1.1
 */
class MetaWidget extends BaseWidget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        echo $this->beforeWidget;

        if ($this->title) {
            echo $this->beforeTitle . $this->title . $this->afterTitle;
        }

        echo Menu::widget([
            'items' => [
                [
                    'label' => 'Site Admin',
                    'url' => Yii::$app->urlManagerBack->baseUrl,
                    'visible' => !Yii::$app->user->isGuest,
                ],
                [
                    'label' => 'Login',
                    'url' => Yii::$app->urlManagerBack->createUrl(['/site/login']),
                    'visible' => Yii::$app->user->isGuest,
                ],
                [
                    'label' => 'Logout',
                    'url' => Yii::$app->urlManager->createUrl(['/site/logout']),
                    'visible' => !Yii::$app->user->isGuest,
                    'template' => '<a href="{url}" data-method="post">{label}</a>',
                ]
            ],
        ]);
        echo $this->afterWidget;
    }
}

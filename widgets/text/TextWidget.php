<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

namespace widgets\text;

use frontend\components\BaseWidget;
use yii\helpers\Html;

/**
 * Class TextWidget
 *
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @since 0.1.1
 */
class TextWidget extends BaseWidget
{
    /**
     * @var string
     */
    public $text = '';

    /**
     * @inheritdoc
     */
    public function run()
    {
        echo $this->beforeWidget;

        if ($this->title) {
            echo $this->beforeTitle . $this->title . $this->afterTitle;
        }

        echo Html::tag('div', $this->text, [
            'class' => 'widget-text',
        ]);
        echo $this->afterWidget;
    }
}

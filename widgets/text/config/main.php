<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

return [
    'title' => 'Text',
    'config' => [
        'class' => 'widgets\text\TextWidget',
        'title' => '',
        'text' => '',
    ],
    'description' => 'Simple widget to show text or HTML.',
    'page' => __DIR__ . '/../views/option.php',
];

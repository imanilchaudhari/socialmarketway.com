<?php

use yii\helpers\Url;
use common\models\Option;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Welcome to '. Option::get("sitetitle");
?>
<section id="banner-container">
    <div class="container">
        <div class="row">
            <div class="banner-text">
                <h1 style="font-size: 32px; color:#fff;">Are You Ready To Generate Customers?
                </h1>
                <!--<span  style="font-size: 31px; color:#36b7d7">Digital MARKETING</span>--> <div class="clearfix"></div>
                <img src="themes/betheme/images1/slider-line.png">
                <p>YOU ARE JUST ONE CLICK AWAY</p>
                <!--<input type="button" class="btn-banner" value="READMORE" name="READ MORE">-->
                <a style=" padding: 10px;"  class="btn-banner"  href="tel:(703)3888843">Talk to Our Experts!</a>
            </div>
        </div>
    </div>
    <span class="skew"></span>
</section>
<section class="about-section" id="about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7 col-xs-12">
                <div class="page-header">
                    <h2>About Us</h2>
                    <img class="heading-image" src="themes/betheme/images1/slider-line.png" />
                </div>
                <p class="heading-bottom">Social Market Way is a leading internet marketing firm built to over-deliver for every client it accepts. We have created a specialized team of SEO specialists and website optimizers . By focusing on new methods, we have found success where others have failed. <p>
                    
                <p class="heading-bottom">
                    Our team is all about being transparent with our customers to getting results. We don’t guarantee rankings, we excel at delivering substantial value for our clients and promise professional services backed by results.
                </p>
            </div>
            <div class="col-md-4 col-sm-5 col-xs-12">
                <div class="form-section">
                    <div role="form" class="wpcf7" id="wpcf7-f3642-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <?php $form = ActiveForm::begin(['id' => 'audit-form', 'method' => 'post', 'options' => ['novalidate' => 'novalidate']]); ?>
                        
                            <h4 class="text-center"> Get Free Business Audit </h4>
                            <ul>
                                <li>
                                    <?= $form->field($model, 'name')->textInput(['placeholder' => 'Name'])->label(false)?>
                                </li>
                                <li>
                                    <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false)?>
                                </li>
                                <li>
                                    <?= $form->field($model, 'phone')->textInput(['placeholder' => 'Phone'])->label(false)?>
                                </li>
                                <li class="submit">
                                    <div class="form-group">
                                        <input type="submit" value="submit" class="wpcf7-form-control wpcf7-submit" />
                                    </div>
                                </li>
                            </ul>
                        <?php $form->end()  ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
            <section id="why-choose-us">
                                        <div class="section mcb-section dark  " style="padding-top:60px; padding-bottom:40px; background-image:url(img/home_agency_section_bg-small-blue.jpg); background-repeat:repeat; background-position:center;"><div class="section_wrapper mcb-section-inner"><div class="wrap mcb-wrap one  valign-top clearfix" style=""><div class="mcb-wrap-inner"><div class="column mcb-column one column_fancy_heading "><div class="fancy_heading fancy_heading_icon"><h2 class="title">Why Choose Us?</h2></div>
                                    </div><div class="column mcb-column one-third column_list "><div class="list_item lists_2 clearfix"><div class="animate zoomIn" data-anim-type="zoomIn"><a href="#"><div class="list_left list_icon"><i class="icon-chart-line"></i></div><div class="list_right"><h4>INCREASE EXPOSURE</h4><div class="desc">Give your business the exposure it deserves by dominating the rankings on Google. We make all digital roads lead to you.</div></div></a></div></div>
                                </div><div class="column mcb-column one-third column_list "><div class="list_item lists_2 clearfix"><div class="animate zoomIn" data-anim-type="zoomIn"><a href="#"><div class="list_left list_icon"><i class="icon-user"></i></div><div class="list_right"><h4>GROW CUSTOMERS</h4><div class="desc">Our services help drive more customers to your website, products, and services than any other online marketing strategies.</div></div></a></div></div>
                            </div><div class="column mcb-column one-third column_list "><div class="list_item lists_2 clearfix"><div class="animate zoomIn" data-anim-type="zoomIn"><a href="#"><div class="list_left list_icon"><i class="icon-chart-bar"></i></div><div class="list_right"><h4>INCREASE REVENUE</h4><div class="desc">Watch as your company grows from a news stream of inquiries or customers who want to purchase your good and services.</div></div></a></div></div>
                        </div><div class="column mcb-column one column_divider "><hr class="no_line">
                        </div><div class="column mcb-column one-third column_list "><div class="list_item lists_2 clearfix"><div class="animate zoomIn" data-anim-type="zoomIn"><a href="#"><div class="list_left list_icon"><i class="icon-thumbs-up-line"></i></div><div class="list_right"><h4>YOUR PARTNER</h4><div class="desc">We are here to help you so whether you have a question, a problem to resolve or in need of an update, we would be happy to hear from you. We are not a faceless organization. You’ll get to know our team.</div></div></a></div></div>
                    </div><div class="column mcb-column one-third column_list "><div class="list_item lists_2 clearfix"><div class="animate zoomIn" data-anim-type="zoomIn"><a href="#"><div class="list_left list_icon"><i class="icon-chart-area"></i></div><div class="list_right"><h4>WEEKLY REPORTS</h4><div class="desc">Weekly reports with rankings updates and noteworthy news, to make clients aware of any algorithm changes in the industry and advising recommendations to help secure your company online.</div></div></a></div></div>
                </div><div class="column mcb-column one-third column_list "><div class="list_item lists_2 clearfix"><div class="animate zoomIn" data-anim-type="zoomIn"><a href="#"><div class="list_left list_icon"><i class="icon-feather"></i></div><div class="list_right"><h4>INGENUITY</h4><div class="desc">Not only are we SEO gurus (humbly of course) but we also belong to mastermind groups that include the world’s best marketers. We discuss strategy, what’s working and what to move away from. We stay on top of our industry to benefit your site.</div></div></a></div></div>
            </div><div class="column mcb-column one column_divider "><hr class="no_line">
            </div><div class="column mcb-column one-third column_list "><div class="list_item lists_2 clearfix"><div class="animate zoomIn" data-anim-type="zoomIn"><a href="#"><div class="list_left list_icon"><i class="icon-chrome"></i></div><div class="list_right"><h4>DOMINATE GOOGLE</h4><div class="desc">We aren’t satisfied with only the number one position. We want to take over the whole front page. We have successfully dominated for the toughest searches in the nation.</div></div></a></div></div>
        </div><div class="column mcb-column one-third column_list "><div class="list_item lists_2 clearfix"><div class="animate zoomIn" data-anim-type="zoomIn"><a href="#"><div class="list_left list_icon"><i class="icon-tools"></i></div><div class="list_right"><h4>DONE IN-HOUSE</h4><div class="desc">No outsourcing, black hat tactics, or cutting corners. All work is done in-house by our team of internet marketing professionals.</div></div></a></div></div>
    </div><div class="column mcb-column one-third column_list ">
        <div class="list_item lists_2 clearfix">
            <div class="animate zoomIn" data-anim-type="zoomIn">
                <a href="#">
                    <div class="list_left list_icon"><i class="icon-doc-line"></i></div>
                    <div class="list_right">
                        <h4>WORRY-FREE CONTRACTS</h4>
                        <div class="desc">
                            Month-to-month contracts. If you’re paying for results and a company doesn’t deliver, why should you be forced to stick around?
        
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</section>

<section id="services-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="services-heading">
                    <h2>What We Offer?</h2>
                    <img class="heading-image" src="themes/betheme/images1/slider-line.png" alt="" />
                    <!--  <p> We provide results you can count on. Our Digital Market Experts will help you to increase your exposure and revenue. As a partner we will help you to grow your customers. We will also provide you weekly reports. </p>-->
                    
                    
                </div>
            </div>
            <div class="col-md-8">
                <div class="services-tab-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">SERVICES</a></li>
                        <li><a data-toggle="tab" href="#menu1">HOW WE CAN HELP</a></li>
                        <li><a data-toggle="tab" href="#menu2">CONTACT US</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <ul class="list-unstyled">
                                <li>SEO(Search Engine Optimization)</li>
                                <li>PPC(Pay Per Click)</li>
                                <li>Facebook Advertisement</li>
                            </ul>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <ul class="list-unstyled">
                                <p class="home-page-content"><li>Higher Page Rankings</li><li>	Increased Traffic</li>
                                    <li>Increased ROI</li>
                                    <li>More Leads/Customers</li></p>
                                </ul>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <ul class="list-unstyled">
                                    <p style=" font-size: 18px;"><b>To get in touch with us please either call us or fill in the quick call back form on the right to see how we can help you.</b>
                                    </p><ul><li>Call us: <strong>(703) 388 8843</strong></li>
                                    </ul><p style="text-align: left;    font-size: 18px;">
                                        Head Office:
                                        
                                        Social Market Way
                                        1450 Irving St NW #43,
                                        Washington, DC 20010
                                        HOURS: 8am - 6pm
                                    </p><p></p>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="background: #36b7d7; margin-top: 40px;">
                    <div class="services-form">
                        <div class="form-heading">
                            <h3 style="margin-bottom: 23px; text-align: center;color:#fff;font-size: 22px;font-weight: 600;">Request a call back</h3>
                        </div>
                        <div class="form">
                            <div role="form" class="wpcf7" id="wpcf7-f3643-o2" lang="en-US" dir="ltr">
                                <div class="screen-reader-response"></div>
                                <?php $form = ActiveForm::begin(['id' => 'service-form', 'method' => 'post', 'options' => ['novalidate' => 'novalidate']]); ?>

                                    <?= $form->field($model, 'name', ['template' => '<span class="wpcf7-form-control-wrap text-252">{input}</span>{error}'])->textInput(['placeholder' => 'Name', 'class' => 'wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control'])->label(false)?>
                                    
                                    <?= $form->field($model, 'email', ['template' => '<span class="wpcf7-form-control-wrap text-252">{input}</span>{error}'])->textInput(['placeholder' => 'Email', 'class' => 'wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control'])->label(false)?>

                                    <?= $form->field($model, 'phone', ['template' => '<span class="wpcf7-form-control-wrap text-252">{input}</span>{error}'])->textInput(['placeholder' => 'Phone', 'class' => 'wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel form-control'])->label(false)?>

                                    <p>
                                        <input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit btn-block btn-service-form" />
                                    </p>
                                <?php $form->end() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial-section">
        <div class="container choose-inner">
            <div class="">
                                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                    <div class="page-header">
                                        <h2>Clients love us</h2>
                                    </div>
                                    <div id="demo2" class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
                                        <div class="span12">
                                            <div id="owl-demo" class="owl-carousel text-left">
                                                <div class="item">
                                                    <div class="slider-content"> <span class="client-number"><img src="themes/betheme/images1/quote-1.png" alt=""/> </span>
                                                        <p>Working with Socialmarketway feel working with a professional  marketing company, it feels as if we have gained an internal team of the company. They provide highly scaled team. The flexibility, agility and scalability are maximum and it is amazingly how open and trusted they are. They are very proactive.
                                                            Socialmarketway is a very professional and expanding company. If you come here it feels like you are a part of the family. They believe in “Work Hard, Party Hard” In short Socialmarketway !</p>
                                                            <span class="client-name">Smith</span></div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="slider-content"> <span class="client-number"><img src="themes/betheme/images1/quote-1.png" alt=""/> </span>
                                                                <p> Great Team to work with, really attentive and react to request immediately. Excellent work and I'm really pleased with the results. Thanks Socialmarketway</p>
                                                                <span class="client-name">John</span></div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="slider-content"> <span class="client-number"><img src="themes/betheme/images1/quote-1.png" alt=""/> </span>
                                                                    <p>
                                                                        I am very impressed with the organization a Socialmarketway. Unlike other SEO companies, with you I know exactly what work is being done, with other companies you just hoped that they were doing what they said they were going to do!</p>
                                                                        <span class="client-name">Paul</span></div>
                                                                    </div>
                                                                    <div class="item">
                                                                        <div class="slider-content"> <span class="client-number"><img src="themes/betheme/images1/quote-1.png" alt=""/> </span>
                                                                            <p>Professional team of Digital Marketing professionals. They know what they are doing. Escalations were handled professionally and pain points were addressed.</p>
                                                                            <span class="client-name">Samuels</span></div>
                                                                        </div>
                                                                        <!--<div class="item">
                                                                            <div class="slider-content"> <span class="client-number"><img src="themes/betheme/images1/quote-1.png" alt=""/> </span>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                                                                                <span class="client-name">Paul Doe <span>Designation</span> </span></div>
                                                                            </div> -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <div class="section mcb-section full-width  " id="MOOSE-BOTTOM-ICON-BLOCK" style="padding-top:100px; padding-bottom:50px; background-color:#167a95; background-image:url(img/home_agency_section_bg-small-blue.jpg); background-repeat:repeat; background-position:center;"><div class="section_wrapper mcb-section-inner"><div class="wrap mcb-wrap one  valign-top clearfix" style=""><div class="mcb-wrap-inner"><div class="column mcb-column one-fourth column_icon_box bottom-info-block-icon"><div class="animate fadeInLeft" data-anim-type="fadeInLeft"><div class="icon_box icon_position_top no_border"><div class="icon_wrapper"><div class="icon"><i class="icon-chrome"></i></div></div><div class="desc_wrapper"><div class="desc"><h2>SEO</h2><img src="img/right-icon-30w.png" style="float: right;">
                                                    <h4>Search Engine Optimization</h4></div></div></div>
                                                </div>
                                            </div><div class="column mcb-column one-fourth column_icon_box bottom-info-block-icon"><div class="animate fadeInUp" data-anim-type="fadeInUp"><div class="icon_box icon_position_top no_border"><a class="" href="#"><div class="icon_wrapper"><div class="icon"><i class="icon-stackoverflow"></i></div></div><div class="desc_wrapper"><div class="desc"><h2>Higher Page Rankings</h2><img src="img/right-icon-30w.png" style="float: right;">
                                                <h4>On Search Engines like Google</h4></div></div></a></div>
                                            </div>
                                        </div><div class="column mcb-column one-fourth column_icon_box bottom-info-block-icon"><div class="animate fadeInDown" data-anim-type="fadeInDown"><div class="icon_box icon_position_top no_border"><a class="" href="#"><div class="icon_wrapper"><div class="icon"><i class="icon-chart-bar"></i></div></div><div class="desc_wrapper"><div class="desc"><h2>Increased Traffic</h2><img src="img/right-icon-30w.png" style="float: right;">
                                            <h4>To your website</h4></div></div></a></div>
                                        </div>
                                    </div><div class="column mcb-column one-fourth column_icon_box middle-info-block-icon"><div class="animate fadeInRight" data-anim-type="fadeInRight"><div class="icon_box icon_position_top no_border"><a class="" href="#"><div class="icon_wrapper"><div class="icon"><i class="icon-money-line"></i></div></div><div class="desc_wrapper"><div class="desc"><h2>Increased ROI</h2>
                                        <h4>Return On Investment</h4></div></div></a></div>
                                    </div>
                                </div></div></div></div></div>
                                <section id="help">
                                    <div class="section mcb-section   " style="padding-top:30px; padding-bottom:0px; background-color:"><div class="section_wrapper mcb-section-inner"><div class="wrap mcb-wrap one  valign-top clearfix" style=""><div class="mcb-wrap-inner"><div class="column mcb-column one-third column_list "><div class="list_item lists_2 clearfix"><div class="animate zoomIn" data-anim-type="zoomIn"><div class="list_left list_icon"><i class=" icon-mail-line"></i></div><div class="list_right"><div class="desc"><big style="padding-top: 12px; display: block">Have any questions?</big>
                                        <h4><a href="mailto:Staff@socialmaketway.com">Staff@socialmaketway.com</a></h4></div></div></div></div>
                                    </div><div class="column mcb-column one-third column_list "><div class="list_item lists_2 clearfix"><div class="animate zoomIn" data-anim-type="zoomIn"><div class="list_left list_icon"><i class=" icon-comment-line"></i></div><div class="list_right"><div class="desc"><big style="padding-top: 12px; display: block">Call us</big>
                                        <h4><a href="#">(703) 388 8843</a></h4></div></div></div></div>
                                    </div><div class="column mcb-column one-third column_list "><div class="list_item lists_2 clearfix"><div class="animate zoomIn" data-anim-type="zoomIn"><div class="list_left list_icon"><i class=" icon-doc-line"></i></div><div class="list_right"><div class="desc"><big style="padding-top: 12px; display: block">Keep In Touch</big>
                                        <h4><a href="/contact/">CONTACT US</a></h4></div></div></div></div>
                                    </div></div></div></div></div></section>
                                    
                                    <section id="call-us-section">
                                        <div class="container">
                                            <div class="row no-padding">
                                                <div class="col-md-1 col-md-offset-2">
                                                    <div class="call-icon">
                                                        <img src="themes/betheme/images1/call-icon.png" alt="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="call-detail">
                                                        <h4>Need help? Call out support team today on</h4>
                                                        <h2><a href="tel:(703)3888843">(703) 388 8843</a></h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>

<header class="dr-header">
    <div class="top-header">
        <div class="container">	
            <div class="row">
                <div class="col-sm-3">
                    <ul class="social-sec">
                        <li><a href="#"><i class="fa fa-rss" aria-hidden="true"></i> </a> </li>
                        <li><a href="http://facebook.com/socialmarketway"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                        <li><a href="https://twitter.com/socialmarketway"><i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
                        <li><a href="https://www.pinterest.com/?autologin=true"><i class="fa fa-pinterest-p" aria-hidden="true"></i> </a> </li>
                    </ul>
                </div>
                <div class="col-sm-9">
                    <ul class="top-ul pull-right">
                        <!-- <li class="free-notice-btn"> <a href="javascript:;" class="btn btn-primary"> Free Evication Notice </a> </li>-->
                        <li class="contact-details"> 
                            <a href="tel:(703) 388 8843"> <span> <i class="fa fa-phone"></i> </span>
                                <p> (703) 388 8843 </p>
                            </a> 
                        </li>
                        <li class="contact-details"> 
                            <a href="mailto:staff@socialmarketway.com "> <span> <i class="fa fa-envelope"></i> </span>
                                <p> staff@socialmarketway.com </p>
                            </a> 
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <a class="navbar-brand" href="#"> <img src="img/logo.png" alt="logo"/></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class=""><a href="/" >Home </a></li>
                    <li><a href="#about-us">About Us</a></li>
                    <li><a href="#why-choose-us">Why Choose Us</a></li>
                    <li class=""><a href="#services-container" >Services</a></li>
                    <li><a href="/blog">Blog</a></li>
                    <li><a href="#help">Help</a></li>
                    <li><a href="#help">Contact Us</a></li>
                </ul>
            </div>
            <!--/.nav-collapse --> 
        </div>
    </nav>
</header>
<footer id="Footer" class="clearfix">
    <div class="widgets_wrapper" style="">
        <div class="container">
            <div class="column one-third">
                <aside id="text-2" class="widget widget_text">
                    <div class="textwidget">
                        <a href="http://facebook.com/socialmarketway" class="icon_bar  icon_bar_facebook icon_bar_small" ><span class="t"><i class="icon-facebook"></i></span><span class="b"><i class="icon-facebook"></i></span></a>
                        
                        <a href="http://twitter.com/socialmarketway" class="icon_bar  icon_bar_twitter icon_bar_small" target="_blank"><span class="t"><i class="icon-twitter"></i></span><span class="b"><i class="icon-twitter"></i></span></a>
                        
                        <a href="https://www.linkedin.com/company/social-market-way" class="icon_bar  icon_bar_linkedin icon_bar_small" target="_blank"><span class="t"><i class="icon-linkedin"></i></span><span class="b"><i class="icon-linkedin"></i></span></a>
                        
                        <a href="http://instagram.com/socialmarketway" class="icon_bar  icon_bar_instagram icon_bar_small" target="_blank"><span class="t"><i class="icon-instagram"></i></span><span class="b"><i class="icon-instagram"></i></span></a>
                        
                        <a href="http://pintrest.com/socialmarketway" class="icon_bar  icon_bar_pinterest icon_bar_small" target="_blank"><span class="t"><i class="icon-pinterest"></i></span><span class="b"><i class="icon-pinterest"></i></span></a>
                        
                        <a href="https://www.youtube.com/channel/UCT3XW3MVd7ZTQj44AR9pN2w" class="icon_bar  icon_bar_youtube icon_bar_small" target="_blank"><span class="t"><i class="icon-youtube"></i></span><span class="b"><i class="icon-youtube"></i></span></a>
                    </div>
                </aside>
                <aside id="media_image-2" class="widget widget_media_image">
                    <img class="image " src="img/logo.png" alt="" width="280" height="75" />
                </aside>
            </div>
            <div class="column one-third">
                <aside id="widget_mfn_menu-2" class="widget widget_mfn_menu">
                    <div class="menu-footer-menu-container">
                        <ul id="menu-footer-menu" class="menu submenus-show">
                            <li id="menu-item-2526" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2526"><a href="about/">About</a></li>
                            <li id="menu-item-2524" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2524"><a href="locations/">Locations</a></li>
                            <li id="menu-item-2535" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2535"><a href="testimonials-2/">Testimonials</a></li>
                            <li id="menu-item-2525" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2525"><a href="blog/">Blog</a></li>
                            <li id="menu-item-2528" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2528"><a href="discovery-page/">Ask For A Quote</a></li>
                        </ul>
                    </div>
                </aside>
            </div>
            <div class="column one-third">
                <aside id="text-4" class="widget widget_text">
                    <div class="textwidget">
                        <h5 style="margin-bottom: 5px;"><i class=" icon-mail-line"></i> Head Office:</h5>
                        <p style="margin-left: 26px;"><strong>Social Market Way</strong><br>1450 Irving St NW #43,<br> Washington, DC 20010<br> HOURS: 8am - 6pm</p>
                        <h5 style="margin-bottom: 5px;"><i class=" icon-phone"></i> Have any questions?</h5>
                        <p style="margin-left: 26px;">
                            <a href="mailto:support@socialmarketway.com">support@socialmarketway.com</a>
                        </p>
                        <h5 style="margin-bottom: 5px;"><i class=" icon-comment-line"></i> Call us:</h5>
                        <p style="margin-left: 26px;"><a href="tel:7033888843">(703) 388 8843</a></p>
                    </div>
                </aside>
            </div>
        </div>
    </div>
    <div class="footer_copy">
        <div class="container">
            <div class="column one">
                <a id="back_to_top" class="button button_left button_js" href=""><span class="button_icon"><i class="icon-up-open-big"></i></span></a>					
                <!-- Copyrights -->
                <div class="copyright">
                    &copy; 2018 Social Market Way. All Rights Reserved.
                </div>
                <ul class="social">
                    <li class="facebook"><a  href="https://www.facebook.com/socialmarketway" title="Facebook"><i class="icon-facebook"></i></a></li>
                    <li class="twitter"><a  href="https://twitter.com/socialmarketway" title="Twitter"><i class="icon-twitter"></i></a></li>
                    <li class="youtube"><a  href="https://www.youtube.com/channel/UCT3XW3MVd7ZTQj44AR9pN2w" title="YouTube"><i class="icon-play"></i></a></li>
                    <li class="linkedin"><a  href="https://www.linkedin.com/company/social-market-way" title="LinkedIn"><i class="icon-linkedin"></i></a></li>
                    <li class="pinterest"><a  href="https://www.pinterest.com/" title="Pinterest"><i class="icon-pinterest"></i></a></li>
                    <li class="instagram"><a  href="https://www.instagram.com/socialmarketway/" title="Instagram"><i class="icon-instagram"></i></a></li>
                </ul>							
            </div>
        </div>
    </div>
</footer>
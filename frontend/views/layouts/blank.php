<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Option;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html>
<head>
	<style>
        
    </style>
    

    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <meta name="google-site-verification" content="B4XOdwWDrAqdCz0ckWujBQW3_32YRpOHlI5L3wAQR5k" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

	<link rel="shortcut icon" type="image/png" href="images1/favicon1.png"/>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,300i,400,500,600,700,800,900" rel="stylesheet">
    
    <!-- Owl Carousel Assets -->
    <link rel='stylesheet' id='rs-plugin-settings-css'  href='plugins/revslider/public/assets/css/settings.css?ver=5.4.7.1' type='text/css' media='all' />

    
    <link rel='stylesheet' id='mfn-animations-css'  href='themes/betheme/assets/animations/animations.min.css?ver=20.8.8' type='text/css' media='all' />
    <link rel='stylesheet' id='mfn-responsive-css'  href='themes/betheme/css/responsive.css?ver=20.8.8' type='text/css' media='all' />
    <link rel='stylesheet' id='Roboto-css'  href='http://fonts.googleapis.com/css?family=Roboto%3A1%2C300%2C400%2C400italic%2C700&#038;ver=4.9.4' type='text/css' media='all' />
    <link rel='stylesheet' id='Lato-css'  href='http://fonts.googleapis.com/css?family=Lato%3A1%2C300%2C400%2C400italic%2C700&#038;ver=4.9.4' type='text/css' media='all' />
    <link rel='stylesheet' id='style-css'  href='themes/SocialMediaWay-betheme-child/style.css?ver=20.8.8' type='text/css' media='all' />
    <!-- style | background -->
    
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    
    <?= $content; ?>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
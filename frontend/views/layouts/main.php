<?php

use frontend\widgets\Alert;
?>

<?php $this->beginContent('@app/views/layouts/blank.php') ?>
	<?= $this->render('header')?>
	<!-- Fixed navbar -->

	<?= Alert::widget() ?>
	
	<?= $content ?>

	<!-- #Footer -->		
	<?= $this->render('footer')?>
<?php $this->endContent() ?>
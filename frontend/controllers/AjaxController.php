<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Ajax controller
 */
class AjaxController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['track', 'index'],
                'rules' => [
                    [
                        'actions' => ['track', 'index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'track' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return [];
    }

    public function actionTrack()
    {
        return [];
    }
}

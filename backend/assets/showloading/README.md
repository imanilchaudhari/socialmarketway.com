
##Configuration

To make the plugin work you just need to register asset bundle. Either specify it as a dependency, or register directly in your view like this:
``` php
ShowLoadingAsset::register($this);
```

##Sample usage

To show / hide loading indicator, simply call `showLoading()` / `hideLoading()` methods:
``` js
$('#my-content-panel-id').showLoading();
$('#my-content-panel-id').hideLoading();
```

> Dual licensed under the MIT and GPL licenses.
<?php
/**
 * @link      http://www.anilchaudhari.com.np/
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license   http://www.anilchaudhari.com.np/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * WidgetAsset is used to register file assets on 'widget' page.
 *
 * @author  Anil Chaudhari <imanilchaudhari@gmail.com>
 * @since   0.1.0
 */
class WidgetAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $depends = [
        'yii\jui\JuiAsset',
        'backend\assets\AppAsset',
    ];

    public function init()
    {
        YII_DEBUG ? $this->js = ['js/widget.js'] : $this->js = ['js/min/widget.js'];
    }
}

<?php

namespace backend\assets;

use Yii;
use yii\web\AssetBundle;

class ShowLoadingAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/showloading';
    public $css = [
        'css/showLoading.css',
    ];
    public $js = [  // Configured conditionally (source/minified) during init()
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public function init()
    {
        parent::init();
        $this->js[] = YII_DEBUG ? 'js/jquery.showLoading.js' : 'js/jquery.showLoading.min.js';
    }
}

<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Contact */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">

    <div class="form-inline grid-nav" role="form">
        <div class="form-group">
            <?= Html::dropDownList('bulk-action', null, [
                'active' => Yii::t('app', 'Active'),
                'not-active' => Yii::t('app', 'Not Active'),
                'deleted' => Yii::t('app', 'Delete Permanently'),
            ], [
                'class' => 'bulk-action form-control',
                'prompt' => Yii::t('app', 'Bulk Action'),
            ]) ?>

            <?= Html::button(Yii::t('app', 'Apply'), ['class' => 'btn btn-flat btn-warning bulk-button']) ?>

            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-search']), [
                'class' => 'btn btn-flat btn-info',
                'data-toggle' => 'collapse',
                'data-target' => '#contact-search',
            ]) ?>

        </div>
    </div>
    <?php Pjax::begin() ?>
    <?= $this->render('_search', ['model' => $searchModel]) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'email:email',
            'phone',
            'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end() ?>

</div>
<?php $this->registerJs('jQuery(document).on("click", ".bulk-button", function(e){
    e.preventDefault();
    if(confirm("' . Yii::t('app', 'Are you sure?') . '")){
        var ids     = $("#contact-grid-view").yiiGridView("getSelectedRows");
        var action  = $(this).closest(".form-group").find(".bulk-action").val();
        $.ajax({
            url: "' . Url::to(["bulk-action"]) . '",
            data: { ids: ids, action: action, _csrf: yii.getCsrfToken() },
            type: "POST",
            success: function(data){
                  $.pjax.reload({container:"#contact-grid-view"});
            }
        });
    }
});') ?>

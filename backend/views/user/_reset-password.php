<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-form">
    <?php $form = ActiveForm::begin(['id' => 'user-reset-password-form']) ?>

    <?= $form->field($model, 'password_old')->passwordInput([
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('password_old'),
    ]) ?>

    <?= $form->field($model, 'password')->passwordInput([
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('password'),
    ]) ?>

    <?= $form->field($model, 'password_repeat')->passwordInput([
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('password_repeat'),
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save my new password'), ['class' => 'btn-flat btn btn-primary']) ?>

    </div>
    <?php ActiveForm::end() ?>

</div>

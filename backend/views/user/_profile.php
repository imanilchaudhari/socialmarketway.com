<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-form">
    <?php $form = ActiveForm::begin(['id' => 'user-profile-form']) ?>

    <?= $form->field($model, 'username')->textInput([
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('username'),
    ]) ?>

    <?= $form->field($model, 'email')->textInput([
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('email'),
    ])->hint(Yii::t('app', 'The email is used for notification and reset password')) ?>

    <?= $form->field($model, 'full_name')->textInput([
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('full_name'),
    ]) ?>

    <?= $form->field($model, 'display_name')->textInput([
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('display_name'),
    ])->hint(Yii::t('app', 'This name will appear on public')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-flat btn-primary']) ?>

    </div>
    <?php ActiveForm::end() ?>

</div>

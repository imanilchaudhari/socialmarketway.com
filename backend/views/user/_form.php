<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-form">
    <?php $form = ActiveForm::begin(['id' => 'user-form']) ?>

    <?= $form->field($model, 'username')->textInput([
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('username'),
    ]) ?>

    <?= $form->field($model, 'email')->input('email', [
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('email'),
    ])->hint(Yii::t('app', 'An e-mail used for receiving notification and resetting password.')) ?>

    <?= $model->isNewRecord ? $form->field($model, 'password')->passwordInput([
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('password'),
    ]) : '' ?>

    <?= $form->field($model, 'full_name')->textInput([
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('full_name'),
    ]) ?>

    <?= $form->field($model, 'display_name')->textInput([
        'maxlength' => 255,
        'placeholder' => $model->getAttributeLabel('display_name'),
    ])->hint(Yii::t('app', 'Display name will be used as your public name.')) ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatuses()) ?>

    <?php
    $role = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'name');
    unset($role['superadmin']);

    if (Yii::$app->user->can('administrator')
        && !Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'superadmin')
    ) {
        unset($role['administrator']);
    }

    echo $form->field($model, 'role')->dropDownList($role);
    ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn-flat btn btn-success' : 'btn-flat btn btn-primary']
        ) ?>
    </div>
    <?php ActiveForm::end() ?>

</div>

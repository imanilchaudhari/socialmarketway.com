<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

/* @var $this yii\web\View */
/* @var $model common\models\Option */

$this->title = Yii::t('app', 'Update Setting: {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Setting'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="option-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

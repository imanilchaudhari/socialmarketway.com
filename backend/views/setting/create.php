<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

/* @var $this yii\web\View */
/* @var $model common\models\Option */

$this->title = Yii::t('app', 'Add New Setting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="option-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

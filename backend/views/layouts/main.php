<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

use codezeen\yii2\adminlte\widgets\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $content string */
?>
<?php $this->beginContent('@app/views/layouts/blank.php') ?>
<div class="wrapper">
    <?= $this->render('main-header') ?>
    <?= $this->render('main-sidebar') ?>
    <div class="content-wrapper">
        <section class="content-header">
            <h1><?= $this->title ?></h1>

            <?= Breadcrumbs::widget([
                'homeLink' => [
                    'label' => Html::a(
                        '<i class="fa fa-dashboard"></i> ' . Yii::t('app', 'Home'),
                        Yii::$app->homeUrl
                    ),
                ],
                'encodeLabels' => false,
                'links' => ArrayHelper::getValue($this->params, 'breadcrumbs', []),
            ]) ?>
        </section>
        <section class="content clearfix">
            <?= Alert::widget() ?>
            <?= $content ?>
        </section>
    </div>
    <?= $this->render('main-footer') ?>
</div>
<?php $this->endContent() ?>

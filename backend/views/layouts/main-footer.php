<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

/* @var $this yii\web\View */
?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b><?= Yii::powered() ?></b>
    </div>
    <strong>
        Copyright &copy; <?= date('Y') ?> <a href="http://www.anilchaudhari.com.np/" target="_blank">Anil Chaudhari</a>.
    </strong>
    All rights reserved.
</footer>

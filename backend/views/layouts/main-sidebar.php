<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

use cebe\gravatar\Gravatar;
use codezeen\yii2\adminlte\widgets\Menu;
use common\models\Option;
use common\models\PostType;
use common\models\VideoType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
?>

<aside class="main-sidebar">
    <section class="sidebar">

        <?php if (!Yii::$app->user->isGuest): ?>
            <div class="user-panel">
                <div class="pull-left image">
                    <?= Gravatar::widget([
                        'email' => Yii::$app->user->identity->email,
                        'options' => ['alt' => 'Gravatar 45', 'class' => 'img-circle'],
                        'size' => 45,
                    ]) ?>
                </div>
                <div class="pull-left info">
                    <p><?= Yii::$app->user->identity->username ?></p>
                    <?= Html::a(
                        '<i class="fa fa-circle text-success"></i>' . Yii::t('app', 'Online'),
                        ['/user/profile']
                    ) ?>
                </div>
            </div>
        <?php endif ?>

        <?php
        $adminMenu[0] = [
            'label' => Yii::t('app', 'MAIN NAVIGATION'),
            'options' => ['class' => 'header'],
            'template' => '{label}',
        ];
        $adminMenu[1] = [
            'label' => Yii::t('app', 'Dashboard'),
            'icon' => 'fa fa-dashboard',
            'url' => ['/site/index']
        ];

        $adminMenu[2] = [
            'label' => Yii::t('app', 'Emails'),
            'icon' => 'fa fa-envelope',
            'url' => ['/contact/index']
        ];
        
        $adminMenu[30] = [
            'label' => Yii::t('app', 'Modules'),
            'icon' => 'fa fa-laptop',
            'items' => [
                [
                    'icon' => 'fa fa-circle-o',
                    'label' => Yii::t('app', 'All Modules'),
                    'url' => ['/module/index'],
                ],
                [
                    'icon' => 'fa fa-circle-o',
                    'label' => Yii::t('app', 'Add New Module'),
                    'url' => ['/module/create'],
                ],
            ],
            'visible' => Yii::$app->user->can('administrator'),
        ];
        
        $adminMenu[60] = [
            'label' => Yii::t('app', 'Users'),
            'icon' => 'fa fa-user',
            'items' => [
                [
                    'icon' => 'fa fa-circle-o',
                    'label' => Yii::t('app', 'All Users'),
                    'url' => ['/user/index'],
                    'visible' => Yii::$app->user->can('administrator'),
                ],
                [
                    'icon' => 'fa fa-circle-o',
                    'label' => Yii::t('app', 'Add New User'),
                    'url' => ['/user/create'],
                    'visible' => Yii::$app->user->can('administrator'),
                ],
                [
                    'icon' => 'fa fa-circle-o',
                    'label' => Yii::t('app', 'My Profile'),
                    'url' => ['/user/profile'],
                    'visible' => Yii::$app->user->can('subscriber'),
                ],
                [
                    'icon' => 'fa fa-circle-o',
                    'label' => Yii::t('app', 'Reset Password'),
                    'url' => ['/user/reset-password'],
                    'visible' => Yii::$app->user->can('subscriber'),
                ],
            ],
        ];
        $adminMenu = ArrayHelper::merge($adminMenu, Option::getMenus(70));

        if ($userAdminMenu = ArrayHelper::getValue(Yii::$app->params, 'adminMenu', [])) {
            $adminMenu = ArrayHelper::merge($adminMenu, $userAdminMenu);
        }

        ksort($adminMenu);
        echo Menu::widget(['items' => $adminMenu]);
        ?>

    </section>
</aside>

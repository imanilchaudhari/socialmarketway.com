<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

use backend\assets\WidgetAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $active [] */
/* @var $available [] */
/* @var $spaces [] */

$this->title = Yii::t('app', 'Widgets');
$this->params['breadcrumbs'][] = $this->title;
WidgetAsset::register($this);
?>
<div class="row">
    <div class="col-sm-push-6 col-md-push-5 col-sm-6 col-md-7">
        <div class="row">
            <?= $this->render('_space', [
                'active' => $active,
                'available' => $available,
                'spaces' => $spaces,
            ]) ?>
        </div>
    </div>
    <div class="col-sm-pull-6 col-md-pull-7 col-sm-6 col-md-5">
        <h4><?= Yii::t('app', 'Available Widgets') ?></h4>

        <p class="description">
            <?= Yii::t('app', 'To activate widget, click on + (plus), choose the location and click activate') ?>
        </p>
        <div class="row">
            <?= $this->render('_available', [
                'available' => $available,
                'spaces' => $spaces,
            ]) ?>
        </div>
        <div class="form-group">
            <?= Html::a(
                Yii::t('app', 'Add New Widget'),
                ['create'],
                ['class' => 'btn btn-default btn-block']
            ) ?>

        </div>
    </div>
</div>

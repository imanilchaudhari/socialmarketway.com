<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

use codezeen\yii2\adminlte\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model common\models\PasswordResetRequestForm */

$this->title = 'Request password reset';
?>
<div class="login-box">
    <div class="login-logo">
        <h1>
            <a href="http://www.anilchaudhari.com.np/">
                <img src="<?= Yii::getAlias('@web/img/logo.png') ?>">
            </a>
        </h1>
    </div>

    <?= Alert::widget() ?>

    <div class="login-box-body">
        <p class="login-box-msg">
            <?= Yii::t('app', 'Please fill out your email. A link to reset password will be sent there.') ?>
        </p>

        <?php $form = ActiveForm::begin(['id' => 'request-password-token-form']) ?>

        <?= $form->field($model, 'email', [
            'template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span></div>{error}',
        ])->textInput(['placeholder' => $model->getAttributeLabel('email')]) ?>

        <div class="form-group">
            <?= Html::submitButton('Send', ['class' => 'btn btn-flat btn-primary form-control']) ?>

        </div>
        <?php ActiveForm::end() ?>

    </div>
</div>

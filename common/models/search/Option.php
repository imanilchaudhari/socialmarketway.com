<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @copyright Copyright (c) 2015 Anil Chaudhari.
 * @license http://www.anilchaudhari.com.np/license/
 */

namespace common\models\search;

use common\models\Option as OptionModel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Option represents the model behind the search form about `common\models\Option`.
 *
 * @author Anil Chaudhari. <imanilchaudhari@gmail.com>
 * @since 0.1.0
 */
class Option extends OptionModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [
                [
                    'name',
                    'value',
                    'label',
                    'group'
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OptionModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'group', $this->group]);


        return $dataProvider;
    }
}

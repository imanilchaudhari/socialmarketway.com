<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "widget".
 *
 * @property int $id
 * @property string $title
 * @property string $config
 * @property string $location
 * @property int $order
 * @property string $directory
 * @property int $created_at
 * @property int $updated_at
 */
class Widget extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widget';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'config', 'location', 'directory', 'created_at', 'updated_at'], 'required'],
            [['config'], 'string'],
            [['order', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['location', 'directory'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'config' => Yii::t('app', 'Config'),
            'location' => Yii::t('app', 'Location'),
            'order' => Yii::t('app', 'Order'),
            'directory' => Yii::t('app', 'Directory'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\Widget the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\Widget(get_called_class());
    }
}

<?php
/**
 * @link https://anilchaudhari.com.np/
 * @copyright Copyright (c) 2017 Anil Chaudhari
 * @license https://anilchaudhari.com.np/license/
 */

namespace common\models;

use common\components\Json;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%module}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $config
 * @property integer $status
 * @property string $directory
 * @property integer $backend_bootstrap
 * @property integer $frontend_bootstrap
 * @property string $created_at
 * @property string $updated_at
 *
 */
class Module extends ActiveRecord
{
    // Constant for module activation.
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;
    // Constant for module backend bootstrapping .
    const BACKEND_BOOTSTRAP = 1;
    const NOT_BACKEND_BOOTSTRAP = 0;
    // Constant for module backend bootstrapping .
    const FRONTEND_BOOTSTRAP = 1;
    const NOT_FRONTEND_BOOTSTRAP = 0;

    /**
     * @var \yii\web\UploadedFile
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%module}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title', 'config', 'directory'], 'required'],
            [['title', 'description', 'config'], 'string'],
            ['name', 'string', 'max' => 64],
            ['directory', 'string', 'max' => 128],
            [['name', 'directory'], 'unique'],
            [['created_at', 'updated_at'], 'safe'],
            [['status', 'backend_bootstrap', 'frontend_bootstrap'], 'integer'],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_NOT_ACTIVE]],
            ['status', 'default', 'value' => self::STATUS_NOT_ACTIVE],
            ['backend_bootstrap', 'in', 'range' => [self::BACKEND_BOOTSTRAP, self::NOT_BACKEND_BOOTSTRAP]],
            ['backend_bootstrap', 'default', 'value' => self::NOT_BACKEND_BOOTSTRAP],
            ['frontend_bootstrap', 'in', 'range' => [self::FRONTEND_BOOTSTRAP, self::NOT_BACKEND_BOOTSTRAP]],
            ['frontend_bootstrap', 'default', 'value' => self::NOT_FRONTEND_BOOTSTRAP],
            ['file', 'required', 'on' => 'create'],
            ['file', 'file', 'extensions' => 'zip'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'config' => Yii::t('app', 'Config'),
            'status' => Yii::t('app', 'Active'),
            'directory' => Yii::t('app', 'Directory'),
            'frontend_bootstrap' => Yii::t('app', 'Is Frontend Bootstrap'),
            'backend_bootstrap' => Yii::t('app', 'Is Backend Bootstrap'),
            'created_at' => Yii::t('app', 'Installed'),
            'updated_at' => Yii::t('app', 'Updated'),
            'file' => Yii::t('app', 'Module (ZIP)'),
        ];
    }

    /**
     * Get module status as array
     */
    public function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Yes'),
            self::STATUS_NOT_ACTIVE => Yii::t('app', 'No'),
        ];
    }

    /**
     * Get array
     */
    public function getBackendBootstraps()
    {
        return [
            self::BACKEND_BOOTSTRAP => Yii::t('app', 'Yes'),
            self::NOT_BACKEND_BOOTSTRAP => Yii::t('app', 'No'),
        ];
    }

    /**
     * Get array
     */
    public function getFrontendBootstraps()
    {
        return [
            self::FRONTEND_BOOTSTRAP => Yii::t('app', 'Yes'),
            self::NOT_FRONTEND_BOOTSTRAP => Yii::t('app', 'No'),
        ];
    }


    /**
     * Get active modules.
     *
     * @return array|Module[]
     */
    public static function getActiveModules()
    {
        return static::find()->where(['status' => self::STATUS_ACTIVE])->all();
    }

    /**
     * Get config as array.
     *
     * @return mixed
     */
    public function getConfig()
    {
        return Json::decode($this->config);
    }

    /**
     * Get module backend config.
     *
     * @return array
     */
    public function getBackendConfig()
    {
        return ArrayHelper::getValue($this->getConfig(), 'backend', []);
    }

    /**
     * Get module frontend config.
     *
     * @return array
     */
    public function getFrontendConfig()
    {
        return ArrayHelper::getValue($this->getConfig(), 'frontend', []);
    }

    /**
     * Get module param path.
     *
     * @return string.
     */
    public function getParamPath()
    {
        return Yii::getAlias('@modules/' . $this->directory . '/config/params.php');
    }

    /**
     * Get module config path.
     *
     * @return string.
     */
    public function getConfigPath()
    {
        return Yii::getAlias('@modules/' . $this->directory . '/config/config.php');
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_at = time();
            }
            $this->updated_at = time();

            return true;
        }

        return false;
    }
}
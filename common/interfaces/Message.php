<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\interfaces;

/**
* Interface Message .
*
* @author Anil Chaudhari <imanilchaudhari@gmail.com>
* @since 0.0.1
*/

interface Message {
    /**
     * Gets the message title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Gets the message description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Gets the message route
     *
     * @return string
     */
    public function getRoute();
}
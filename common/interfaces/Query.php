<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\interfaces;

/**
* Interface Query .
*
* @author Anil Chaudhari <imanilchaudhari@gmail.com>
* @since 0.0.1
*/

interface Query {
    /**
     * Gets Only Published Items
     *
     * @return string
     */
    public function published();

    /**
     * Gets With Published Items
     *
     * @return string
     */
    public function withPublished();

    /**
     * Gets Only Unpublished Items
     *
     * @return string
     */
    public function unpublished();

    /**
     * Gets With Unpublished Items
     *
     * @return string
     */
    public function withUnpublished();

    /**
     * Gets Only Deleted items
     *
     * @return string
     */
    public function deleted();

    /**
     * Gets With Undeleted Items
     *
     * @return string
     */
    public function withDeleted();

    /**
     * Gets Active Only Items [ it must address to nt deleted items ]
     *
     * @return string
     */
    public function activeOnly();
}
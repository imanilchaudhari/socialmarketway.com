<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @copyright Copyright (c) 2015 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

namespace common\components;

use Yii;
use common\models\Module;
use common\models\Option;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\helpers\ArrayHelper;

/**
 * Class BackendBootstrap
 *
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @since 0.1.0
 */
class BackendBootstrap implements BootstrapInterface
{
    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        $this->setTime($app);
        $this->setModule($app);
    }

    /**
     * Set time base on Option.
     *
     * @param Application $app the application currently running
     */
    protected function setTime($app)
    {
        /* TIME ZONE */
        $app->timeZone = Option::get('time_zone');
        /* DATE TIME */
        $app->formatter->dateFormat = 'php:' . Option::get('date_format');
        $app->formatter->timeFormat = 'php:' . Option::get('time_format');
        $app->formatter->datetimeFormat = 'php:' . Option::get('date_format') . ' ' . Option::get('time_format');
    }

    /**
     * Set modules.
     *
     * @param Application $app the application currently running
     */
    protected function setModule($app)
    {
        foreach (Module::getActiveModules() as $module) {
            // Get module backend config.
            if ($config = $module->getBackendConfig()) {
                // Set module.
                $app->setModules([$module->name => $config]);
                // Merge application params with exist module params.
                if (is_file($module->getParamPath())) {
                    $params = require($module->getParamPath());
                    if ($backendParams = ArrayHelper::getValue($params, 'backend')) {
                        $app->params = ArrayHelper::merge($app->params, $backendParams);
                    }
                }
                // Bootstrap injection.
                if ($module->backend_bootstrap) {
                    $component = $app->getModule($module->name);
                    if ($component instanceof BootstrapInterface) {
                        $component->bootstrap($app);
                    }
                }
            }
        }
    }
}

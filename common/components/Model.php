<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\components;

use Yii;

/**
 * Model
 */
class Model extends \yii\db\ActiveRecord
{
	public function init()
	{
		parent::init();
		// Stuff Goes Here
	}
}
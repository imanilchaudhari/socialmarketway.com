<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @copyright Copyright (c) 2015 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

namespace common\components;

use yii\base\Object;

/**
 * Class BaseWidget
 *
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @since 0.2.0
 */
abstract class BaseWidget extends Object
{
    /**
     * @var integer Id of active widget that can be used for id of HTML element.
     */
    public $id;
    /**
     * @var string
     */
    public $title = '';

    /**
     * @var string
     */
    public $beforeTitle = '';

    /**
     * @var string
     */
    public $afterTitle = '';

    /**
     * @var string
     */
    public $beforeWidget = '';

    /**
     * @var string
     */
    public $afterWidget = '';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->run();
    }

    /**
     * Executes the widget.
     */
    public function run()
    {
    }
}

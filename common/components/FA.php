<?php
/**
 * @link http://www.anilchaudhari.com.np/
 * @copyright Copyright (c) 2016 Anil Chaudhari
 * @license http://www.anilchaudhari.com.np/license/
 */

namespace common\components;

/**
* Class for fontawesome using fontasesome assets
*/
class FA extends \rmrevin\yii\fontawesome\FA {

}
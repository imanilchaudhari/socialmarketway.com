<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

/**
 * String Helper extended from yii\helpers\StringHelper
 *
 */
class StringHelper extends \yii\helpers\StringHelper
{
    /**
     * Gets random string.
     *
     * @param integer $length           String length.
     * @param integer $encryption_level Encryption level.
     *
     * @return string
     */
    public static function getRandomString($length = null, $encryption_level = 5)
    {
        if ($length == null || intval($length) <= 0) {
            return null;
        }

        $str = null;
        for ($i = 0; $i < $encryption_level; $i++)
        {
            $str = base64_encode(md5(microtime(true)) . $str);
        }
        return substr($str, 0, $length);
    }

    /**
     * Get chopped string.
     * @param string $str
     * @param int $length
     * @param string $suffix
     * @return string
     */
    public static function getChoppedString($str, $length, $suffix = '..')
    {
        if (strlen($str) > $length) {
            return mb_substr($str, 0, $length -2) . $suffix;
        }
        return $str;
    }

    /**
     * Convert string to array
     * @param string $str
     * @return array
     */
    public static function convertToArray($str)
    {
        if (is_string($str)) {
            if (strpos($str, ',') > 0) {
                $data = explode(',', $str);
            } else {
                $data = [$str];
            }
        }
        else
        {
            $data = [];
        }
        return $data;
    }

    /**
     * Replace back slash by forward slash.
     * @param type $string
     * @return type
     */
    public static function replaceBackSlashByForwardSlash($string)
    {
        return str_replace('\\', '/', $string);
    }

    /**
     * compute diff
     *
     */
    public static function computeDiff($from, $to){
        $diffValues = [];
        $diffMask = [];

        $dm = [];
        $n1 = count($from);
        $n2 = count($to);

        for ($j = -1; $j < $n2; $j++) $dm[-1][$j] = 0;
        for ($i = -1; $i < $n1; $i++) $dm[$i][-1] = 0;
        for ($i = 0; $i < $n1; $i++)
        {
            for ($j = 0; $j < $n2; $j++)
            {
                if ($from[$i] == $to[$j])
                {
                    $ad = $dm[$i - 1][$j - 1];
                    $dm[$i][$j] = $ad + 1;
                }
                else
                {
                    $a1 = $dm[$i - 1][$j];
                    $a2 = $dm[$i][$j - 1];
                    $dm[$i][$j] = max($a1, $a2);
                }
            }
        }

        $i = $n1 - 1;
        $j = $n2 - 1;
        while (($i > -1) || ($j > -1))
        {
            if ($j > -1)
            {
                if ($dm[$i][$j - 1] == $dm[$i][$j])
                {
                    $diffValues[] = $to[$j];
                    $diffMask[] = 1;
                    $j--;
                    continue;
                }
            }
            if ($i > -1)
            {
                if ($dm[$i - 1][$j] == $dm[$i][$j])
                {
                    $diffValues[] = $from[$i];
                    $diffMask[] = -1;
                    $i--;
                    continue;
                }
            }
            {
                $diffValues[] = $from[$i];
                $diffMask[] = 0;
                $i--;
                $j--;
            }
        }

        $diffValues = array_reverse($diffValues);
        $diffMask = array_reverse($diffMask);

        return ['values' => $diffValues, 'mask' => $diffMask];
    }

    /**
	 * Replaces the given words with a string.
	 *
	 *     // Displays "What the #####, man!"
	 *     echo StringHelper::censor('What the frick, man!', array(
	 *         'frick' => '#####',
	 *     ));
	 *
	 * @param   string  $str                    phrase to replace words in
	 * @param   array   $badwords               words to replace
	 * @param   string  $replacement            replacement string
	 * @param   boolean $replace_partial_words  replace words across word boundries (space, period, etc)
	 * @return  string
	 * @uses    UTF8::strlen
	 */
	public static function censor($str, $badwords, $replacement = '#', $replace_partial_words = TRUE)
	{
		foreach ( (array) $badwords as $key => $badword)
		{
			$badwords[$key] = str_replace('\*', '\S*?', preg_quote( (string) $badword));
		}

		$regex = '('.implode('|', $badwords).')';

		if ($replace_partial_words === FALSE) {
			// Just using \b isn't sufficient when we need to replace a badword that already contains word boundaries itself
			$regex = '(?<=\b|\s|^)'.$regex.'(?=\b|\s|$)';
		}

		$regex = '!'.$regex.'!ui';

		if (strlen($replacement) == 1) {
			$regex .= 'e';
			return preg_replace($regex, 'str_repeat($replacement, strlen(\'$1\'))', $str);
		}

		return preg_replace($regex, $replacement, $str);
	}
}
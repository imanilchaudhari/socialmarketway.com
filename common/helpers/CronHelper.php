<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

class CronHelper
{
    public static $CommonSetting = array(
        '* * * * *' => 'Once Per Minute(* * * * *)',
        '*/5 * * * *' => 'Once Per Five Minutes(*/5 * * * *)',
        '0,30 * * * *' => 'Twice Per Hour(0,30 * * * *)',
        '0 * * * *' => 'Once Per Hour(0 * * * *)',
        '0 0,12 * * *' => 'Twice Per Day(0 0,12 * * *)',
        '0 0 * * *' => 'Once Per Day(0 0 * * *)',
        '0 0 * * 0' => 'Once Per Week(0 0 * * 0)',
        '0 0 1,15 * *' => 'On the 1st and 15th of the Month(0 0 1,15 * *)',
        '0 0 1 * *' => 'Once Per Month(0 0 1 * *)',
        '0 0 1 1 *' => 'Once Per Year(0 0 1 1 *)'
    );

    public static function GetCommonSettingName($key){
      $array = self::$CommonSetting;
      return $array[$key] == true ? $array[$key] : 'Custome Configured';
    }
}

<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

 /**
 * Class TimeZoneHelper, used as List of timezones with GMT offset. .
 *
 * @author Anil Chaudhari <imanilchaudhari@gmail.com>
 * @since 0.0.1
 */
class TimeZoneHelper
{
    /**
     * List of timezone as array.
     *
     * @return array
     */
    public static function listTimeZone()
    {
        $timezone = [];
        $timestamp = time();

        foreach (timezone_identifiers_list() as $zone) {
            date_default_timezone_set($zone);
            $timezone[$zone] = $zone . ' UTC/GMT ' . date('P', $timestamp);
        }

        return $timezone;
    }
}

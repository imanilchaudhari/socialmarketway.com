<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

/**
 * NumberHelper class file.
 *
 * @package common\helpers
 */
class NumberHelper
{
    /**
     * Compare float
     * @param float $a
     * @param float $b
     * @return boolean
     */
    public static function compareFloat($a, $b)
    {
        $epsilon = 0.00001;
        if(abs($a - $b) < $epsilon)
        {
            return true;
        }
        return false;
    }
}
<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

use Yii;
use yii\helpers\Json;

/**
 * CacheHelper class file.
 * 
 * @package common\helpers
 */
class CacheHelper
{
    /**
     * Check if can connect to database or not.
     * @param string $host
     * @param string $port
     * @return mixed $error
     */
    public static function checkMemcacheConnection($host, $port)
    {
        $errorNumber    = 0;
        $errorString    = '';
        $timeout        = 2;
        $connection     = @fsockopen($host, $port, $errorNumber, $errorString, $timeout);
        if ($connection !== false) {
            fclose($connection);
            return true;
        }
        return array($errorNumber, $errorString);
    }

    /**
     * Clears the cache.
     * @return void.
     */
    public static function clearCache()
    {
        Yii::$app->cache->flush();
    }

    /**
     * Sets the cache.
     * @param mixed $key a key identifying the value to be cached. This can be a simple string or
     * a complex data structure consisting of factors representing the key.
     * @param mixed $value the value to be cached
     * @param integer $duration the number of seconds in which the cached value will expire. 0 means never expire.
     * @param Dependency $dependency dependency of the cached item. If the dependency changes,
     * the corresponding value in the cache will be invalidated when it is fetched via [[get()]].
     */
    public static function set($key, $value, $duration = 0, $dependency = null)
    {
        $key = Yii::$app->language . '_' . $key;
        Yii::$app->cache->set($key, $value, $duration, $dependency);
    }

    /**
     * Retrieves a value from cache with a specified key.
     * @param mixed $key a key identifying the cached value. This can be a simple string or
     * a complex data structure consisting of factors representing the key.
     * @return mixed the value stored in cache, false if the value is not in the cache, expired,
     * or the dependency associated with the cached data has changed.
     */
    public static function get($key)
    {
        $key = Yii::$app->language . '_' . $key;
        return Yii::$app->cache->get($key);
    }
    
    /**
     * Delete a value from cache with a specified key.
     * @param mixed $key a key identifying the cached value. This can be a simple string or
     * a complex data structure consisting of factors representing the key.
     */
    public static function delete($key)
    {
        $key = Yii::$app->language . '_' . $key;
        Yii::$app->cache->delete($key);
    }
    
    /**
     * Sets model cache.
     * @param string $modelClassName
     * @param string $key
     * @see TranslationTrait::loadTranslation
     * @return void
     */
    public static function setModelCache($modelClassName, $key)
    {
        $modelCacheKey = Yii::$app->getObjectClassName($modelClassName) . 'Cache';
        $value = static::get($modelCacheKey);
        if($value === false) {
            static::set($modelCacheKey, Json::encode([$key]));
        } else {
            $data = Json::decode($value);
            if (!in_array($key, $data)) {
                $data[] = $key;
                static::set($modelCacheKey, Json::encode($data));
            }
        }
    }
    
    /**
     * Loads configuration.
     * 
     * @return void
     */
    public static function loadConfiguration()
    {
        $configData = [];
    }
}

<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

use common\models\Notification;
use yii\base\Exception;
use yii\db\Expression;

/**
* Class NotofocationHelper
*
* @author Anil Chaudhari
* @source https://github.com/machour/yii2-notifications
* @since 0.0.1
*/

class NotificationHelper extends \yii\base\Object
{

    /**
     * @var callable|integer The current user id
     */
    public $receiver_id;

    /**
     * @inheritdoc
     */
    public function init() {
        $this->receiver_id = Yii::$app->user->id;
        parent::init();
    }

    /**
     * Creates a notification
     *
     * @param Notification $notification The notification class
     * @param string $key The notification key
     * @param integer $receiver_id The user id that will get the notification
     * @param integer $key_id The key unique id
     * @param string $type The notification type
     * @return bool Returns TRUE on success, FALSE on failure
     * @throws Exception
     */
    public static function notify($notification, $key, $key_id = null, $key_type, $receiver_id, $receiver_type, $sender_id, $sender_type,  $type = Notification::TYPE_DEFAULT)
    {



        if (!in_array($key, $notification::$keys)) {
            throw new Exception("Not a registered notification key: $key");
        }

        if (!in_array($type, Notification::$types)) {
            throw new Exception("Unknown notification type: $type");
        }

        /** @var Notification $instance */
        $instance = $notification::findOne(['receiver_id' => $receiver_id, 'receiver_type' => 1, 'key' => $key, 'key_id' => $key_id]);
        if (!$instance) {
            $instance = new $notification([
                'key' => $key,
                'key_id' => $key_id,
                'key_type' => $key_type,

                'type' => $type,

                'receiver_id' => $receiver_id,
                'receiver_type' => $receiver_type,

                'sender_id' => $sender_id,
                'sender_type' => $sender_type,



                'seen' => 0,
                'created_at' => new Expression('NOW()'),
            ]);
            return $instance->save(false);
        }
        else{
            $instance->updateAttributes(['seen' => 0]);
        }
        return true;
    }
}
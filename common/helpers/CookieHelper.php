<?php
/**
 * @copyright Copyright (C) 2017 Yii2 Codes.
 * @license https://www.gnu.org/licenses/gpl-3.0.html
 */
namespace common\helpers;

use Yii;
/**
 * Class consisting of helper functions related to cookie.
 *
 * @package common\helpers
 */
class CookieHelper
{
    /**
     * Get value of the cookie.
     * @param string $cookieName
     * @return Cookie the cookie with the specified name. Null if the named cookie does not exist.
     */
    public static function getValue($cookieName)
    {
        return Yii::$app->getRequest()->getCookies()->get($cookieName);
    }

    /**
     * Remove cookie.
     * @param string $cookieName
     * @return void
     */
    public static function remove($cookieName)
    {
        Yii::$app->getResponse()->getCookies()->remove($cookieName);
    }

    /**
     * Remove all cookies.
     * @return void
     */
    public static function removeAllCookies()
    {
        Yii::$app->getResponse()->getCookies()->removeAll();
    }
}
<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

use Yii;
/**
 * DateTime helper methods.
 * @package common\helpers
 */
class DateTimeHelper
{
    /**
     * Gets formatted date time.
     *
     * @param string $timestamp unix timestamp
     *
     * @return formatted date time
     */

    public static function getDatetime($timestamp)
    {
        return Yii::$app->formatter->asDatetime($timestamp);
    }

    /**
     * Gets formatted date time.
     *
     * @param string $dateTime  Datetime.
     * @param string $dateWidth Date width format.
     * @param string $timeWidth Time width format.
     *
     * @return null
     */
    public static function getFormattedDateTime($dateTime, $format = 'medium')
    {
        if ($dateTime != '0000-00-00 00:00:00' and $dateTime != null)
        {
            if(is_integer($dateTime))
            {
                return Yii::$app->formatter->asDatetime($dateTime, $format);
            }
            return Yii::$app->formatter->asDatetime(strtotime($dateTime), $format);
        }
        elseif ($dateTime == null || $dateTime == '0000-00-00 00:00:00')
        {
            return $dateTime = Yii::t('app', '(not set)');
        }
        return $dateTime;
    }

    /**
     * Gets formatted date.
     *
     * @param string $dateTime  Datetime.
     * @param string $dateWidth Date width format.
     * @param string $timeWidth Time width format.
     *
     * @return null
     */
    public static function getFormattedDate($dateTime, $format = 'medium')
    {
        if ($dateTime != '0000-00-00 00:00:00' and $dateTime != null)
        {
            if(is_integer($dateTime))
            {
                return Yii::$app->formatter->asDate($dateTime, $format);
            }
            return Yii::$app->formatter->asDate(strtotime($dateTime), $format);
        }
        elseif ($dateTime == null || $dateTime == '0000-00-00 00:00:00')
        {
            return $dateTime = Yii::t('app', '(not set)');
        }
        return $dateTime;
    }

    /**
     * Get timeago based on timestamp
     * @param $timestamp
     * @return date time in string
     */
    public static function timeago($timestamp)
    {
        $estimate_time = time() - $timestamp;

        if ($estimate_time < 1) {
            return 'a second ago';
        }

        $condition = [
                    12 * 30 * 24 * 60 * 60  =>  'year',
                    30 * 24 * 60 * 60       =>  'month',
                    24 * 60 * 60            =>  'day',
                    60 * 60                 =>  'hour',
                    60                      =>  'minute',
                    1                       =>  'second'
        ];

        foreach ($condition as $secs => $str) {
            $d = $estimate_time / $secs;

            if ($d >= 1) {
                $r = round($d);
                return 'about ' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
            }
        }
    }
}
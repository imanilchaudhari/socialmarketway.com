<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

/**
 * Class FileHelper extends \yii\helpers\FileHelper.
 */
class FileHelper extends \yii\helpers\FileHelper
{
    /**
     * Creates a new directory with path symlinks.
     * See BaseFileHelper::createDirectory() for details.
     *
     * @param array $symlinks list of symlinks to be traveresed or created.
     * List of key => value pairs where key is directory basename and value is symlink target.
     * Symlink target path will be created using createDirectory() if necessary.
     * @param string $path path of the directory to be created.
     * @param integer $mode the permission to be set for the created directory.
     * @param boolean $recursive whether to create parent directories if they do not exist.
     * @return boolean whether the directory is created successfully
     * @throws \yii\base\Exception if the directory could not be created (i.e. php error due to parallel changes)
     */
    public static function createSymDirectory($symlinks, $path, $mode = 0775, $recursive = true)
    {
        if (is_dir($path)) {
            return true;
        }
        $parentDir = dirname($path);
        // recurse if parent dir does not exist and we are not at the root of the file system.
        if ($recursive && !is_dir($parentDir) && $parentDir !== $path) {
            static::createSymDirectory($symlinks, $parentDir, $mode, true);
        }
        try {
            $symlink = ArrayHelper::remove($symlinks, basename($path));
            if (!empty($symlink)) {
                if (!is_dir($symlink)) {
                    if (!static::createDirectory($symlink, $mode, $recursive)) {
                        return false;
                    }
                }
                if (!symlink($symlink, $path)) {
                    return false;
                }
            } elseif (!mkdir($path, $mode)) {
                return false;
            }
        } catch (\Exception $e) {
            if (!is_dir($path)) {// https://github.com/yiisoft/yii2/issues/9288
                throw new \yii\base\Exception("Failed to create directory \"$path\": " . $e->getMessage(), $e->getCode(), $e);
            }
        }
        try {
            return chmod($path, $mode);
        } catch (\Exception $e) {
            throw new \yii\base\Exception("Failed to change permissions for directory \"$path\": " . $e->getMessage(), $e->getCode(), $e);
        }
    }

   /**
     *
     * @param type $directory
     * @return type
     */
    public static function scanDirectory($directory)
    {
        $files = [];
        // Is there actually such a folder/file?
        if (is_dir($directory)) {
            foreach (scandir($directory) as $f) {
                if (!$f || $f[0] == '.') {
                    continue; // Ignore hidden files
                }
                if (is_dir($directory . '/' . $f)) {
                    // The path is a folder
                    $files[] = [
                        "name" => $f,
                        "type" => "folder",
                        "path" => $directory . '/' . $f,
                        "items" => self::scanDirectory($directory . '/' . $f) // Recursively get the contents of the folder
                    ];
                } else {
                    // It is a file
                    $files[] = [
                        "name" => $f,
                        "type" => "file",
                        "path" => $directory . '/' . $f,
                        "size" => filesize($directory . '/' . $f) // Gets the size of this file
                    ];
                }
            }
        }
        return $files;
    }


    /**
     *
     * @param type $src
     * @param type $dest
     * @return type
     */
     //http://php.net/manual/fr/function.copy.php/#Hcom88520
    public static function stream_copy($src, $dest)
    {
        $fsrc = fopen($src, 'r');
        $fdest = fopen($dest, 'w+');
        $len = stream_copy_to_stream($fsrc, $fdest);
        fclose($fsrc);
        fclose($fdest);
        return $len;
    }


    public static function recurse_copy($src, $dest)
    {
        $dir = opendir($src);
        @mkdir($dest);
        while (false !== ($file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file)) {
                    self::recurse_copy($src . '/' . $file, $dest . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dest . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    public static function shell_copy($src, $dest)
    {
        if (!is_dir($dest)) {
            @mkdir($dest, 0777, true);
        }
        $command = "cp -a $src $dest";
        shell_exec($command);
    }
}
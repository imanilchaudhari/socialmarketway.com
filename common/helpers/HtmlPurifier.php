<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

/**
 * Class HtmlPurifier extends \yii\helpers\HtmlPurifier.
 *
 */
class HtmlPurifier extends \yii\helpers\HtmlPurifier
{

}
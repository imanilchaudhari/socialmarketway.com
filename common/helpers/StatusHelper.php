<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

use Yii;

/**
 * StatusHelper class file.
 *
 * @package common\helpers
 */
class StatusHelper extends Status
{
    /**
     * Active status constant.
     */
    const STATUS_INACTIVE           = 0;
    const STATUS_ACTIVE             = 1;
}
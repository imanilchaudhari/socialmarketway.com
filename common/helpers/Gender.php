<?php

namespace common\helpers;

use Yii;

/**
 * Gender
 */

abstract class Gender
{
    const MALE = 1;
    const FEMALE = 2;
    const OTHER = 3;

    public static function all()
    {
        return [
            self::MALE => Yii::t('app', 'Male'),
            self::FEMALE => Yii::t('app', 'Female'),
            self::OTHER => Yii::t('app', 'Other'),
        ];
    }

    public static function get($gender)
    {
        $all = self::all();

        if (isset($all[$gender])) {
            return $all[$gender];
        }

        return Yii::t('app', 'Not set');
    }
}

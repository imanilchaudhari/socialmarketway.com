<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

use Yii;
use yii\helpers\Url;

/**
 * Class consisting of helper functions related to request.
 *
 * @package common\helpers
 */
class RequestHelper
{
    /**
     * Gets default host information.
     * @return string
     */
    public static function getDefaultHostInfo()
    {
        $hostInfo = "";
        if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] !='')
        {
            $hostInfo = 'http://' . $_SERVER['HTTP_HOST'];
        }
        elseif (isset($_SERVER['SERVER_NAME']) && $_SERVER['SERVER_NAME'] != '')
        {
            $hostInfo = 'http://' . $_SERVER['SERVER_NAME'];
        }
        return $hostInfo;
    }

    /**
     * Gets default script url
     * @param string $route
     * @return string
     */
    public static function getDefaultScriptUrl($route = '')
    {
        if (isset($_SERVER['PHP_SELF']))
        {
            $url = rtrim($_SERVER['PHP_SELF'], '/');

            $route = rtrim($route, '/');
            if ($route != '')
            {
                $pos = strpos($url, $route);
                if($pos > 0)
                {
                    $url = substr($url, 0, $pos);
                }
                $url = rtrim($url, '/');
            }
            $indexPos = strpos($url, '/index.php');
            if($indexPos > 0)
            {
                $url = substr($url, 0, $indexPos);
            }
            return $url;
        }
        else
        {
            return '';
        }
    }

    /**
     * Get domain url
     * @return string
     */
    public static function getDomainUrl()
    {
        $hostInfo = Yii::$app->request->getHostInfo();
        if(strpos($hostInfo, 'localhost') !== false)
        {
            $url = \yii\helpers\Url::to('@web');
        }
        else
        {
            $url = '/';
        }
        //if access from backend in case of preview
        if(strpos($url, 'admin') > 0)
        {
            $url = str_replace('/admin', '', $url);
        }
        return $url;
    }
}
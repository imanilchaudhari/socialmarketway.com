<?php
/**
* @link https://anilchaudhari.com.np/
* @copyright Copyright (c) 2017 Anil Chaudhari
* @license https://anilchaudhari.com.np/license/
*/

namespace common\helpers;

use Yii;

/**
* Class Status
* @since 0.0.1
*/

abstract class Status
{
    const YES       =   1;
    const NO        =   0;

    const INACTIVE  =   0;
    const ACTIVE    =   1;

    const ALIVE     =   0;
    const DIED      =   1;

    const UNPUBLISHED   =   0;
    const PUBLISHED     =   1;

    const NORMAL    =   0;
    const URGENT    =   1;

    const NEW       =   0;
    const REWRITE   =   1;

    /**
     * Gets label for the status.
     * @param string $model ActiveRecord of the model.
     * @return string
     */
    public static function getLabel($model)
    {
        if (is_array($model)) {
            $model = (object) $model;
        }

        if ($model->status == Status::ACTIVE) {
            return Yii::t('app', 'Active');
        } else if ($model->status == Status::INACTIVE) {
            return Yii::t('app', 'Inactive');
        }
    }

    /**
     * Gets label for the status.
     * @param string $model ActiveRecord of the model.
     * @return string
     */
    public static function getBadge($model)
    {
        if (is_array($model)) {
            $model = (object) $model;
        }

        if ($model->status == Status::ACTIVE) {
            return Yii::t('app', '<span class="badge badge-success">Active</span>');
        } else if ($model->status == Status::INACTIVE) {
            return Yii::t('app', '<span class="badge badge-danger">Inactive</span>');
        }
    }

    /**
     * Gets status dropdown.
     * @return array
     */
    public static function getDropdown()
    {
        return array(
            Status::ACTIVE     => Yii::t('app', 'Active'),
            Status::INACTIVE   => Yii::t('app', 'Inactive')
        );
    }

    /**
     * list of labels for is_died values
     * @return array
     */
    public static function isDiedLabels()
    {
        return [ Status::ALIVE => 'Alive', Status::DIED => 'Dead'];
    }
}